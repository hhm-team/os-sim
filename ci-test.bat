call run-test.bat "test/commands"
call run-test.bat "test/commands-empty-file"
call run-test.bat "test/commands-small-file"
call run-test.bat "test/commands-edge-cases"
call run-test.bat "test/ntfs-general"
call run-test.bat "test/ntfs-dir-tree"
call run-test.bat "test/ntfs-continuous-allocation"
call run-test.bat "test/ntfs-mft-items-order"
call run-test.bat "test/ntfs-trimming"