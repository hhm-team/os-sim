#include "file_system.h"

size_t __stdcall dir(const kiv_hal::TRegisters &regs)
{
	const kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);
	kiv_os::NOS_Error result;
	
	const size_t file_name_size = 12;
	char working_dir[file_name_size];
	memset(working_dir, '\0', file_name_size);

	if ((result = kiv_os_rtl::Get_Working_Dir(working_dir, file_name_size)) == kiv_os::NOS_Error::Success)
	{
		// Podarilo se ziskat nazev pracovniho adresare.

		// Jestlize se nejedna o root slozku, dostat se do slozky rodice, ze ktere pak budeme otevirat pro cteni TDir_Entry nami vybranou slozku.
		bool rollback = false;
		if (working_dir[0] != '/')
		{
			kiv_os_rtl::Set_Working_Dir("..");
			rollback = true;
		}
		
		kiv_os::THandle handle;
		if ((result = kiv_os_rtl::Open_File(working_dir, kiv_os::NOpen_File::fmOpen_Always, handle)) == kiv_os::NOS_Error::Success)
		{
			// Slozka bylo otevrena ke cteni.

			std::string output;
			
			// Cist TDir_Entry polozku po polozce, dokud se nedostaneme na konec slozky.
			int counter = 0;
			while (true)
			{
				kiv_os::TDir_Entry item;
				size_t read;

				if ((result = kiv_os_rtl::Read_File(handle, reinterpret_cast<char *>(&item), 1, read)) == kiv_os::NOS_Error::Success && read > 0)
				{
					if (counter > 0) {
						output += '\n';
					}
					output += item.file_name;
					for (size_t i = strlen(item.file_name); i < 11 /* nazev */ + 4 /* separator */; i++) {
						output += ' ';
					}
					output += std::to_string(item.file_attributes);

					counter++;
				}
				else
				{
					break;
				}
			}

			// Vypsat do konzole vytvoreny buffer.
			size_t written; // zbytecny
			kiv_os_rtl::Write_File(out, output.c_str(), output.length(), written);

			kiv_os_rtl::Close_Handle(handle); // Ikdyby se nepovedlo, u vypisu polozek nevadi, handle se uvolni jinde.
		}

		if (rollback)
		{
			// Jestlize jsme menili pracovni adresar na rodice, pred ukoncenim zmenit zpet.
			kiv_os_rtl::Set_Working_Dir(working_dir);
		}
	}

	kiv_os_rtl::Close_Handle(out);
	kiv_os_rtl::Exit(result);
	return 0;
}

size_t __stdcall md(const kiv_hal::TRegisters &regs)
{
	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);
	kiv_os_rtl::Close_Handle(in_handle);
	kiv_os_rtl::Close_Handle(out_handle);

	const char *directory_name = reinterpret_cast<const char *>(regs.rdi.r);

	kiv_os::THandle handle = kiv_os::Invalid_Handle;
	kiv_os::NOS_Error result;

	if ((result = kiv_os_rtl::Create_File(directory_name, kiv_os::NFile_Attributes::Directory, handle)) == kiv_os::NOS_Error::Success)
	{
		// Pokud vytvoreni slozky probehne v poradku, uvolnit vygenerovany handle.
		kiv_os_rtl::Close_Handle(handle);
	}

	kiv_os_rtl::Exit(result);
	return 0;
}

size_t __stdcall rd(const kiv_hal::TRegisters &regs)
{
	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);
	kiv_os_rtl::Close_Handle(in_handle);
	kiv_os_rtl::Close_Handle(out_handle);

	const char *directory_name = reinterpret_cast<const char *>(regs.rdi.r);
	kiv_os_rtl::Exit(kiv_os_rtl::Delete_File(directory_name));
	return 0;
}