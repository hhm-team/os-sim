#include "parser.h"
#include <iostream>
#include <sstream>
#include "utils.h"

/*
Vlozi precteny retezec do prikazu.

@param command - pointer na prikaz
@param step - aktualni krok
@param ss - std::stringstream
*/
void add_string(parser::command* command, parser::Step & step, std::stringstream & ss) {
	
	switch (step) {
		case parser::Step::Command:
			if (!ss.str().empty()) {
				if (ss.str().compare(FIND) == 0) {
					command->command = "wc";
				}
                else if (ss.str().compare(TASKLIST) == 0) {
                    command->command = "ps";
                }
				else {
					command->command = ss.str();
				}
				step = parser::Step::Arg;
			}
			break;

		case parser::Step::Arg:
			if (!ss.str().empty() && command->params.compare("") == 0) {
				command->params = ss.str();
			}
			break;

		case parser::Step::File_in:
			if (!ss.str().empty()) {
				command->file_name_in = ss.str();
			}
			break;

		case parser::Step::File_out:
			if (!ss.str().empty()) {
				command->file_name_out = ss.str();
			}
			break;
	}

	ss.str("");
}

/*
Inicialiuzuje prikaz.

@return - instance command
*/
parser::command* init_command() {
	parser::command* command = new parser::command();
	command->command = "";
	command->file_name_in = "";
	command->file_name_out = "";
	command->in = parser::StreamType::STD;
	command->out = parser::StreamType::STD;
	command->params = "";

	return command;
}

/*
Zpracuje vstupni radku.

@param buff - vstupni radka
@return - vector instanci command
*/
std::vector<parser::command*> parser::parse_commands(char* buff) {
	std::vector<parser::command*> vec;

	parser::command* command = init_command();

	parser::Step step = parser::Step::Command;
	std::stringstream ss;
	bool arg_start = false;

	for (int i = 0; i < strlen(buff); i++) {
		if (buff[i] == ' ' && !arg_start) {
			add_string(command, step, ss);
		}

		else if (buff[i] == '\"') {
			if (!arg_start) {
				arg_start = true;
			}

			else {
				arg_start = false;
			}
		}

		else if (buff[i] == '<') {
			add_string(command, step, ss);
			step = parser::Step::File_in;
			command->in = StreamType::FILE;
		}

		else if (buff[i] == '>') {
			add_string(command, step, ss);
			step = parser::Step::File_out;
			command->out = StreamType::FILE;
		}

		else if (buff[i] == '|') {
			add_string(command, step, ss);
			command->out = parser::StreamType::PIPE;
			vec.push_back(command);
			command = init_command();
			command->in = parser::StreamType::PIPE;

			step = parser::Step::Command;
		}

		else {
			ss << buff[i];
		}
	}

	add_string(command, step, ss);
	vec.push_back(command);

	return vec;
}

/*
Uvolni instance z pameti.
*/
void parser::free_commands(std::vector<parser::command*> vec)
{
	for (auto it = vec.begin(); it != vec.end(); ++it) {
		delete *it;
	}
}