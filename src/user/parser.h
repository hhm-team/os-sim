#pragma once

#include <vector>
#include <string>

namespace parser {

	enum class StreamType {
		STD,
		PIPE,
		FILE
	};

	enum class Step {
		Command,
		Arg,
		File_in,
		File_out
	};

	struct command {
		std::string command;
		std::string params;
		std::string file_name_in;
		std::string file_name_out;
		StreamType in;
		StreamType out;
	};

	/*
	Zpracuje vstupni radku.

	@param buff - vstupni radka
	@return - vector instanci command
	*/
	std::vector<parser::command*> parse_commands(char* buff);

	/*
	Uvolni instance z pameti.
	*/
	void free_commands(std::vector<parser::command*> vec);
}