#include "echo.h"
#include <thread>
#include <chrono>
#include <sstream>

#include "rtl.h"

size_t __stdcall echo(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	if (in_handle == 0) { 
		size_t counter;

		char* arg = reinterpret_cast<char*>(regs.rdi.r);
		std::stringstream str_stream;
		str_stream << arg;
		auto x = str_stream.str();
		const char* str = x.c_str();

		kiv_os_rtl::Write_File(out_handle, str, strlen(str), counter);
	}

	else { 
		const size_t buff_size = 1024;
		char buff[buff_size];
		size_t count = 0;
		size_t read_count;

		do {
			kiv_os_rtl::Read_File(in_handle, buff, buff_size, count);

			read_count = count;
			if (count > 0) {
				kiv_os_rtl::Write_File(out_handle, buff, read_count, count);
			}
		} while (count != 0);
	}

	kiv_os_rtl::Close_Handle(in_handle);
	kiv_os_rtl::Close_Handle(out_handle);
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);

	return 0;	
}