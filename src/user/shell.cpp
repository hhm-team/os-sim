#include "shell.h"
#include "parser.h"
#include "utils.h"

#include "rtl.h"

bool run = true;
bool read_exit_code = true;
bool show_prompt = true;

/*
Vypsani error zpravy.
*/
void print_error(const char* msg, kiv_os::THandle out_handle) {
	size_t counter;

	kiv_os_rtl::Write_File(out_handle, msg, strlen(msg), counter);
	const char* new_line = "\n";
	kiv_os_rtl::Write_File(out_handle, new_line, strlen(new_line), counter);
}

/*
Vytvori retezec pro zacatek radky v shellu.
@param char *prompt - pripraveny buffer.
@param const size_t prompt_size - velikost pripraveneho bufferu.
*/
void create_prompt(char *prompt, const size_t prompt_size)
{
	memset(prompt, '\0', prompt_size);
	if (show_prompt) {
		size_t written;
		kiv_os_rtl::Get_Working_Dir_Path(prompt, prompt_size, written);
	}
}

/*
Zjisteni error zpravy dle erroru.

@param err - error code
*/
const char* resolve_error(kiv_os::NOS_Error err) {
	switch (err) {

	case kiv_os::NOS_Error::Directory_Not_Empty:
		return DIRECTORY_NOT_EMPTY;

	case kiv_os::NOS_Error::File_Not_Found:
		return FILE_NOT_FOUND;

	case kiv_os::NOS_Error::Invalid_Argument:
		return INVALID_ARG;

	case kiv_os::NOS_Error::IO_Error:
		return IO_ERROR;

	case kiv_os::NOS_Error::Not_Enough_Disk_Space:
		return NOT_ENOUGH_DISK_SPACE;

	case kiv_os::NOS_Error::Out_Of_Memory:
		return OUT_OF_MEMORY;

	case kiv_os::NOS_Error::Permission_Denied:
		return PERMISSION_DENIED;

	default:
		return UNKNOWN_ERROR;
	}
}

/*
Startovani procesu.

@param com - prikaz
@param in_handle - handle pro vstup
@param out_handle - handle pro vystup
@param - read_exit - indikace zda cist exit code
@param - prompt - hodnota prompt
@param - prompt_size - velikost prompt
@return - handle procesu
*/
kiv_os::THandle start_program(parser::command* com, kiv_os::THandle in_handle, kiv_os::THandle out_handle, bool read_exit,
	char *prompt, const size_t prompt_size) {

	kiv_os::THandle ret = kiv_os::Invalid_Handle;

	if (com->command.compare(ECHO_ON_OFF) == 0) {
		if (com->params.compare(PARAMETER_ON) == 0) {
			show_prompt = true;
			create_prompt(prompt, prompt_size);
		}
		else if (com->params.compare(PARAMETER_OFF) == 0) {
			show_prompt = false;
			create_prompt(prompt, prompt_size);
		}
		else {
			print_error(resolve_error(kiv_os::NOS_Error::Invalid_Argument), out_handle);
		}
		read_exit_code = false;
	}
	else if (com->command.compare(CD) == 0) {
		// Pohyb ve FS.
		auto err = kiv_os_rtl::Set_Working_Dir(com->params.c_str());
		if (err == kiv_os::NOS_Error::Success) {
			// Pokud byl zadan spravny parametr, upravit zacatek radky v shellu.
			create_prompt(prompt, prompt_size);
		} else {
			print_error(resolve_error(err), out_handle);
		}
		read_exit_code = false;
	}
#ifdef _DEBUG
	else if (com->command.compare(NTFS_DEBUG_COMMAND) == 0) {
		kiv_os_rtl::NTFS_Debug_Command(com->params.c_str());
		read_exit_code = false;
	}
#endif
	else {
		ret = kiv_os_rtl::Create_Process(com->command.c_str(), com->params.c_str(), in_handle, out_handle);

		if (ret == kiv_os::Invalid_Handle && read_exit) {
			print_error(PROC_ERR_NOT_FOUND, out_handle);
			return kiv_os::Invalid_Handle;
		}

		if (read_exit) {
			kiv_os::NOS_Error exit_code = static_cast<kiv_os::NOS_Error>(kiv_os_rtl::Read_Exit_Code(ret));


			if (exit_code != kiv_os::NOS_Error::Success) {
				print_error(resolve_error(exit_code), out_handle);
				return kiv_os::Invalid_Handle;
			}
		}
	}

	return ret;
}

/*
Handle signalu terminate.
*/
size_t __stdcall handle_term(const kiv_hal::TRegisters &regs) {
	run = false;
	return 0;
}

/*
Uvolneni poli handlu z pameti.

@param vec - vektor poli handlu
@param �lose - indikace zda handli i zavirat
*/
void free_handles(std::vector<kiv_os::THandle*> vec, bool close) {
	for (int i = 0; i < vec.size(); i++) {
		if (close) {
			kiv_os_rtl::Close_Handle(vec[i][0]);
			kiv_os_rtl::Close_Handle(vec[i][1]);
		}

		delete[] vec[i];
	}

	vec.clear();
}

/*
Vypsani file erroru.

@param err - chybova zprava
@param file - jmeno souboru
*/
void print_file_err(const char* err, const char* file, kiv_os::THandle std_out) {
	size_t counter;
	const char* new_line = "\n";

	kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
	kiv_os_rtl::Write_File(std_out, file, strlen(file), counter);
	kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);
}

size_t __stdcall shell(const kiv_hal::TRegisters &regs) {
	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	kiv_os_rtl::Signals_Register(handle_term, kiv_os::NSignal_Id::Terminate);

	const size_t buffer_size = 256;
	char buffer[buffer_size];
	size_t counter;

	const size_t prompt_size = 1024;
	char prompt[prompt_size];
	create_prompt(prompt, prompt_size);

	// Kontrola existence FS.
	const size_t wd_name_size = 12;
	char wd_name[wd_name_size];
	kiv_os_rtl::Get_Working_Dir(wd_name, wd_name_size);

	if (strlen(wd_name) < 1)
	{
		// V pripade, ze disk nebyl nalezen, dojde k vypsani chybove hlasky a k ukonceni shellu.
		const char *disk_error = "Disk not found";
		kiv_os_rtl::Write_File(std_out, disk_error, strlen(disk_error), counter);
	}
	else
	{
		do {
			kiv_os_rtl::Write_File(std_out, prompt, strlen(prompt), counter);

			if (counter == 0) {
				break;
			}
			if (kiv_os_rtl::Read_File(std_in, buffer, buffer_size, counter) == kiv_os::NOS_Error::Success && (counter > 0)) {
				if (counter == buffer_size) counter--;
				buffer[counter] = 0;	//udelame z precteneho vstup null-terminated retezec

				const char* new_line = "\n";
				kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);

				auto commands = parser::parse_commands(buffer);
				read_exit_code = true;
				std::vector<kiv_os::THandle> programs = {};
				std::vector<kiv_os::THandle*> all_handles = {};
				std::vector<kiv_os::THandle*> pipes = {};
				kiv_os::THandle last_handle = kiv_os::Invalid_Handle;
				bool ok = true;
				kiv_os_rtl::Last_Error.store(kiv_os::NOS_Error::Success);

				for (int i = 0; i < commands.size(); i++) {
					kiv_os::THandle* handles = new kiv_os::THandle[2];

					//handle in
					if (i == 0) {
						//stdin
						if (commands[i]->in == parser::StreamType::STD) {
							handles[0] = std_in;
						}

						//file
						else if (commands[i]->in == parser::StreamType::FILE) {
							kiv_os::THandle file;
							if (kiv_os_rtl::Open_File(commands[i]->file_name_in.c_str(), kiv_os::NOpen_File::fmOpen_Always, file) == kiv_os::NOS_Error::Success) {
								handles[0] = file;
							}

							else {
								//print_file_err(OPEN_FILE_ERR, commands[i]->file_name_in.c_str(), std_out);
								ok = false;
								delete[] handles;
								break;
							}
						}

						else {
							//TODO kravina prvni muze mit jenom file nebo std
						}
					}

					//jako in je read pipe z minule pipe
					else {
						handles[0] = last_handle;
					}

					//handle out
					//stdout
					if (commands[i]->out == parser::StreamType::STD) {
						handles[1] = std_out;
						//konci se, protoze pokud ma out na std musi byt posledni
						all_handles.push_back(handles);
						break;
					}

					//file
					else if (commands[i]->out == parser::StreamType::FILE) {
						kiv_os::THandle file;
						kiv_os_rtl::Delete_File(commands[i]->file_name_out.c_str());
						if (kiv_os_rtl::Create_File(commands[i]->file_name_out.c_str(), static_cast<kiv_os::NFile_Attributes>(0), file) == kiv_os::NOS_Error::Success) {
							handles[1] = file;
						}

						else {
							print_file_err(OPEN_FILE_ERR, commands[i]->file_name_out.c_str(), std_out);
							ok = false;
							kiv_os_rtl::Close_Handle(handles[0]);
							delete[] handles;
							break;
						}
					}

					//pipe
					else {
						kiv_os::THandle* arr = new kiv_os::THandle[2];

						if (kiv_os_rtl::Create_Pipe(arr) == kiv_os::NOS_Error::Success) {
							handles[1] = arr[0];
							last_handle = arr[1];
							pipes.push_back(arr);
						}

						else {
							//print_error(OPEN_PIPE_ERR, std_out);
							ok = false;
							delete[] handles;
							break;
						}
					}

					all_handles.push_back(handles);
				}

				if (!ok) {
					if (kiv_os_rtl::Last_Error != kiv_os::NOS_Error::Success) {
						print_error(resolve_error(kiv_os_rtl::Last_Error), std_out);
					}
					free_handles(all_handles, true);
					free_handles(pipes, false);
				}

				else {
					for (int i = 0; i < all_handles.size(); i++) {
						kiv_os::THandle handle = start_program(commands[i], all_handles[i][0], all_handles[i][1], false, prompt, prompt_size);

						if (handle != kiv_os::Invalid_Handle) {
							programs.push_back(handle);
						}

						else {
							ok = false;
							break;
						}
					}

					if (ok) {
						for (int i = 0; i < programs.size(); i++) {
							kiv_os::NOS_Error exit_code = static_cast<kiv_os::NOS_Error>(kiv_os_rtl::Read_Exit_Code(programs[i]));

							if (exit_code != kiv_os::NOS_Error::Success && read_exit_code) {
								print_error(resolve_error(exit_code), std_out);
							}
						}
					}

					else {
						//kvuli ntfs pri debug
						if (read_exit_code) {
							print_error(PROC_ERR_NOT_FOUND, std_out);
						}
					}

					free_handles(all_handles, true);
					free_handles(pipes, false);
				}

				parser::free_commands(commands);

				kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);
			}
			else
				break;	//EOF
		} while (run);
	}

	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);

	return 0;
}