#include "wc.h"
#include "rtl.h"

size_t __stdcall wc(const kiv_hal::TRegisters &regs) {
	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	size_t lines = 1;

	const size_t buff_size = 1024;
	char buff[buff_size];

	size_t bytes_read = 0;
	do {
		kiv_os_rtl::Read_File(in_handle, buff, buff_size, bytes_read);
		if (bytes_read == 0) break;
		
		for (size_t i = 0; i < bytes_read; i++) {
			if (buff[i] == '\n') {
				lines++;
			}
		}
	} while (bytes_read != 0);
	kiv_os_rtl::Close_Handle(in_handle);

	// print words
	snprintf(buff, buff_size, "%zd", lines);
	size_t count;
	kiv_os_rtl::Write_File(out_handle, buff, strlen(buff), count);
	kiv_os_rtl::Close_Handle(out_handle);

	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;
}
