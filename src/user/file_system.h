#pragma once

#include "..\api\api.h"
#include "rtl.h"

// Program pro vypis obsahu adresare.
size_t __stdcall dir(const kiv_hal::TRegisters &regs);

// Program pro vytvoreni adresare.
size_t __stdcall md(const kiv_hal::TRegisters &regs);

// Program pro odstraneni prazdneho adresare (funguje i na soubory).
size_t __stdcall rd(const kiv_hal::TRegisters &regs);