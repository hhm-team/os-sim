#pragma once

#include <string>

static std::string ECHO = "echo";
static std::string ECHO_ON_OFF = "@echo";
static std::string PARAMETER_ON = "on";
static std::string PARAMETER_OFF = "off";
static std::string CD = "cd";
static std::string DIR = "dir";
static std::string MD = "md";
static std::string RD = "rd";
static std::string TYPE = "type";
static std::string SORT = "sort";
static std::string PS = "ps";
static std::string FIND = "find";
static std::string TASKLIST = "tasklist";
static std::string SHUTDOWN = "shutdown";
static std::string NTFS_DEBUG_COMMAND = "ntfs";
static std::string PIPE = "|";
static std::string REDIR_IN = "<";
static std::string REDIR_OUT = ">";
static std::string SPACE = " ";

static const char* PROC_ERR_NOT_FOUND = "Command not found";
static const char* PROC_ERR_ERR = "Error in program";
static const char* INVALID_ARG = "Invalid argument";
static const char* FILE_NOT_FOUND = "File not found";
static const char* DIRECTORY_NOT_EMPTY = "Directory not empty";
static const char* NOT_ENOUGH_DISK_SPACE = "Not enough disk space";
static const char* OUT_OF_MEMORY = "Out of memory";
static const char* PERMISSION_DENIED = "Permission denied";
static const char* IO_ERROR = "IO error";
static const char* UNKNOWN_ERROR = "Unknown error";
static const char* OPEN_FILE_ERR = "Couldn't open file ";
static const char* OPEN_PIPE_ERR = "Couldn't open pipe";

static const char* PROCFILE = "\\.procfile";

std::string& ltrim(std::string& str, const std::string& chars = "\t\n\v\f\r ");
std::string& rtrim(std::string& str, const std::string& chars = "\t\n\v\f\r ");
std::string& trim(std::string& str, const std::string& chars = "\t\n\v\f\r ");

#ifdef _DEBUG
#define LOGGER(f_, ...) printf((f_), ##__VA_ARGS__)
#else
#define LOGGER(f_, ...)
#endif // _DEBUG
