#include "freq.h"
#include <sstream>
#include <iomanip>
#include <map>

#include "rtl.h"

bool print_table(std::map<unsigned char, int> table, kiv_os::THandle out_handle) {
	std::stringstream ss;

	for (auto it = table.begin(); it != table.end(); ) {
		ss << "0x" << std::setfill('0') << std::setw(2) << std::hex << (unsigned int)it->first;
		ss << " : " << std::dec << it->second;
		++it;
		if (it != table.end()) {
			ss << std::endl;
		}
	}
	
	auto x = ss.str();
	const char* buff = x.c_str();
	const size_t buff_size = strlen(buff);
	size_t count;
	kiv_os_rtl::Write_File(out_handle, buff, buff_size, count);

	return (count == buff_size);
}

void add_to_table(std::map<unsigned char, int>& table, char* buff, size_t buff_size) {
	for (int i = 0; i < buff_size; i++) {
		if (buff[i] == '\0') {
			break;
		}

		table[buff[i]]++;
	}
}

size_t __stdcall freq(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::map<unsigned char, int> table = {};

	if (in_handle == 0) { //STDIN - data jsou v argumentu

		char* arg = reinterpret_cast<char*>(regs.rdi.r);

		if (strcmp(arg, "") == 0) {
			kiv_os_rtl::Exit(kiv_os::NOS_Error::Invalid_Argument);
			return 0;
		}

		add_to_table(table, arg, strlen(arg));
	}

	else {

		const size_t buff_size = 1024;
		char buff[buff_size];
		size_t count, read_count;
		do {
			kiv_os_rtl::Read_File(in_handle, buff, buff_size, count);

			read_count = count;
			if (count > 0) {
				add_to_table(table, buff, read_count);
			}
		} while (count > 0);
	}
	
	bool ret = print_table(table, out_handle);

	kiv_os_rtl::Close_Handle(in_handle);
	kiv_os_rtl::Close_Handle(out_handle);
	if (ret) {
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	}

	else {
		kiv_os_rtl::Exit(kiv_os::NOS_Error::IO_Error);
	}
	return 0;	
}