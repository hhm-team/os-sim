#include "rtl.h"

std::atomic<kiv_os::NOS_Error> kiv_os_rtl::Last_Error;

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor) {
	kiv_hal::TRegisters regs;
	memset(&regs, 0, sizeof(kiv_hal::TRegisters));
	regs.rax.h = static_cast<uint8_t>(major);
	regs.rax.l = minor;
	return regs;
}



kiv_os::NOS_Error kiv_os_rtl::Create_File(const char *file_name, kiv_os::NFile_Attributes attributes, kiv_os::THandle &file_handle)
{
	return Create_Open_File(file_name, static_cast<kiv_os::NOpen_File>(-1), attributes, file_handle);
}

kiv_os::NOS_Error kiv_os_rtl::Open_File(const char *file_name, kiv_os::NOpen_File flags, kiv_os::THandle &file_handle)
{
	return Create_Open_File(file_name, flags, static_cast<kiv_os::NFile_Attributes>(-1), file_handle);
}

kiv_os::NOS_Error kiv_os_rtl::Create_Open_File(const char *file_name, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, kiv_os::THandle &file_handle)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Open_File));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(flags);
	regs.rdi.r = static_cast<decltype(regs.rdi.r)>(attributes);

	if (kiv_os::Sys_Call(regs))
	{
		file_handle = regs.rax.x;
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		file_handle = kiv_os::Invalid_Handle;
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Write_File(kiv_os::THandle file_handle, const char *buffer, size_t buffer_size, size_t &written)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File));
	
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(buffer_size);

	if (kiv_os::Sys_Call(regs))
	{
		written = regs.rax.r;
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Read_File(kiv_os::THandle file_handle, char *buffer, size_t buffer_size, size_t &read)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_File));
	
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(buffer_size);

	if (kiv_os::Sys_Call(regs))
	{
		read = regs.rax.r;
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Seek(kiv_os::THandle file_handle, size_t offset, kiv_os::NFile_Seek attribute, size_t *position)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = static_cast<decltype(regs.rdi.r)>(offset);
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(attribute);
	
	if (kiv_os::Sys_Call(regs))
	{
		if (position != nullptr && attribute == kiv_os::NFile_Seek::Get_Position)
		{
			*position = static_cast<decltype(*position)>(regs.rax.r);
		}
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Close_Handle(kiv_os::THandle file_handle)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Close_Handle));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);

	if (kiv_os::Sys_Call(regs))
	{
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Delete_File(const char *file_name)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_File));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);

	if (kiv_os::Sys_Call(regs))
	{
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Set_Working_Dir(const char *dir_path)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Set_Working_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(dir_path);

	if (kiv_os::Sys_Call(regs))
	{
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Get_Working_Dir(char *buffer, const size_t buffer_size)
{
	size_t written; // zbytecne
	return Get_Working_Dir_Path(buffer, buffer_size, written);
}

kiv_os::NOS_Error kiv_os_rtl::Get_Working_Dir_Path(char *buffer, size_t buffer_size, size_t &written)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Get_Working_Dir));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(buffer_size);

	if (kiv_os::Sys_Call(regs))
	{
		written = regs.rax.r;
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}

kiv_os::NOS_Error kiv_os_rtl::Create_Pipe(kiv_os::THandle *array)
{
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Pipe));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(array);

	if (kiv_os::Sys_Call(regs))
	{
		return kiv_os::NOS_Error::Success;
	}
	else
	{
		return static_cast<kiv_os::NOS_Error>(regs.rax.r);
	}
}



kiv_os::THandle kiv_os_rtl::Create_Process(const char * process, kiv_os::THandle in, kiv_os::THandle out) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(process);

	kiv_os::Sys_Call(regs);

	if (regs.flags.carry != 1) {
		kiv_os::THandle result = regs.rax.x;

		return result;
	}

	else {
		return kiv_os::Invalid_Handle;
	}
}

kiv_os::THandle kiv_os_rtl::Create_Process(const char * process, const char* arg, kiv_os::THandle in, kiv_os::THandle out) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(process);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(arg);

	kiv_os::Sys_Call(regs);

	kiv_os::THandle result = regs.rax.x;

	if (regs.flags.carry != 1) {
		return result;
	}

	else {
		return kiv_os::Invalid_Handle;
	}
}

kiv_os::THandle kiv_os_rtl::Create_Thread(void * data, kiv_os::TThread_Proc proc, kiv_os::THandle in, kiv_os::THandle out) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rbx.e = (in << 16) | out;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(kiv_os::NClone::Create_Thread);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(proc);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(data);

	kiv_os::Sys_Call(regs);

	kiv_os::THandle result = regs.rax.x;

	if (regs.flags.carry != 1) {
		return result;
	}

	else {
		return kiv_os::Invalid_Handle;
	}
}

bool kiv_os_rtl::Signals_Register(kiv_os::TThread_Proc handler, kiv_os::NSignal_Id id) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Register_Signal_Handler));
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(id);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(handler);

	const bool result = kiv_os::Sys_Call(regs);

	return result;
}

bool kiv_os_rtl::Shutdown() {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Shutdown));

	bool result = kiv_os::Sys_Call(regs);
	return result;
}

bool kiv_os_rtl::Exit(kiv_os::NOS_Error exit_code) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Exit));
	regs.rcx.x = static_cast<decltype(regs.rcx.x)>(exit_code);

	bool result = kiv_os::Sys_Call(regs);
	return result;
}

kiv_os::NOS_Error kiv_os_rtl::Read_Exit_Code(kiv_os::THandle handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(handle);
	kiv_os::Sys_Call(regs);

	return static_cast<kiv_os::NOS_Error>(regs.rcx.x);
}

bool kiv_os_rtl::Wait_For(std::vector<kiv_os::THandle> handles) {
	bool result = false; 
	uint64_t signalized_count = 0;
	kiv_os::THandle signalized_handle = kiv_os::Invalid_Handle;
	uint64_t count = handles.size();
	kiv_os::THandle* old_arr = new kiv_os::THandle[count];

	std::copy(handles.begin(), handles.end(), old_arr);

	uint64_t old_count = count;


	while (1) {
		kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Wait_For));
		regs.rcx.r = static_cast<decltype(regs.rcx.r)>(old_count);
		regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(old_arr);
		
		result = kiv_os::Sys_Call(regs);

		if (!result) {
			delete[] old_arr;
			return false;
		}

		else {
			signalized_handle = static_cast<kiv_os::THandle>(regs.rax.x);

			if (signalized_handle == kiv_os::Invalid_Handle) {
				delete[] old_arr;
				return false;
			}

			else {
				++signalized_count;

				if (signalized_count != count) {
					uint64_t new_count = old_count - 1;
					kiv_os::THandle* new_arr = new kiv_os::THandle[new_count];

					uint64_t i = 0;
					uint64_t j = 0;
					for ( ; i < old_count; i++) {
						if (old_arr[i] != signalized_handle) {
							new_arr[j] = old_arr[i];
							j++;
						}
					}

					delete[] old_arr;
					old_arr = new_arr;
					old_count = new_count;
				}

				else {
					break;
				}
			}
		}
	}

	delete[] old_arr;
	return result;
}

bool kiv_os_rtl::NTFS_Debug_Command(const char* command) {
	kiv_hal::TRegisters regs;
	regs.rax.r = 3;
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(command);

	return kiv_os::Sys_Call(regs);
}
