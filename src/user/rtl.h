#pragma once

#include "..\api\api.h"
#include "parser.h"
#include <atomic>
#include <vector>

namespace kiv_os_rtl {

	extern std::atomic<kiv_os::NOS_Error> Last_Error;



	// --- FUNKCE PRO VYKONANI SYSTEMOVEHO VOLANI SOUBOROVEHO SYSTEMU ---
	// vytvoreno podle api.h. Kazda funkce vraci konkretni chybovy stav, ke kteremu behem systemoveho volani doslo.

	// !!! Pokud NFile_Attributes = 0, normalni soubor pro cteni i zapis !!!
	// !!! NOpen_File pouze 0 nebo 1 !!!
	
	// Kdyz NOpen_File = -1 => vytvoreni souboru.
	kiv_os::NOS_Error Create_File(const char *file_name, kiv_os::NFile_Attributes attributes, kiv_os::THandle &file_handle);
	
	// Kdyz NFile_Attributes = -1 => otevreni souboru.
	kiv_os::NOS_Error Open_File(const char *file_name, kiv_os::NOpen_File flags, kiv_os::THandle &file_handle);

	// Spolecna funkce pro Create_File a Open_File.
	kiv_os::NOS_Error Create_Open_File(const char *file_name, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, kiv_os::THandle &file_handle);

	// Zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
	kiv_os::NOS_Error Write_File(kiv_os::THandle file_handle, const char *buffer, size_t buffer_size, size_t &written);
	
	// Opacna funkce v Write_File.
	kiv_os::NOS_Error Read_File(kiv_os::THandle file_handle, char *buffer, size_t buffer_size, size_t &read);
	
	// Nastavi pozici (offset) v danem otevrenem souboru (podle file_handle).
	kiv_os::NOS_Error Seek(kiv_os::THandle file_handle, size_t offset, kiv_os::NFile_Seek attribute, size_t *position = nullptr);

	// Uzavre otevreny soubor (dojde k odstraneni z registru otevrenych souboru a k uvolneni handelu).
	kiv_os::NOS_Error Close_Handle(kiv_os::THandle file_handle);

	// Odstrani soubor s danym nazvem (muze byt i relativni cesta) vuci pracovnimu adresari.
	kiv_os::NOS_Error Delete_File(const char *file_name);

	// Zmeni pracovni adresar, viz. funkce shellu CD.
	kiv_os::NOS_Error Set_Working_Dir(const char *dir_path);

	// Zjistli nazev pracovniho adresare (char *).
	kiv_os::NOS_Error Get_Working_Dir(char *buffer, size_t buffer_size);

	// Zjisti cestu k pracovnimu adresari (char *).
	kiv_os::NOS_Error Get_Working_Dir_Path(char *buffer, size_t buffer_size, size_t &written);

	// Vytvori rouru - array[0] = in, array[1] = out.
	kiv_os::NOS_Error Create_Pipe(kiv_os::THandle *array);



	kiv_os::THandle Create_Process(const char* process, kiv_os::THandle in, kiv_os::THandle out);

	kiv_os::THandle Create_Process(const char* process, const char* arg, kiv_os::THandle in, kiv_os::THandle out);

	kiv_os::THandle Create_Thread(void* data, kiv_os::TThread_Proc proc, kiv_os::THandle in, kiv_os::THandle out);

	bool Signals_Register(kiv_os::TThread_Proc handler, kiv_os::NSignal_Id id);

	bool Shutdown();

	bool Exit(kiv_os::NOS_Error exit_code);

	kiv_os::NOS_Error Read_Exit_Code(kiv_os::THandle handle);

	bool Wait_For(std::vector<kiv_os::THandle> handles);

	bool NTFS_Debug_Command(const char* command);

}