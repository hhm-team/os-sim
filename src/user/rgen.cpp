#include "rgen.h"
#include <thread>
#include <chrono>
#include "utils.h"

#include "rtl.h"

std::atomic<bool> write;

size_t __stdcall handle_sigterm(const kiv_hal::TRegisters &regs) {
	write.store(false);

	return 0;
}

size_t __stdcall ctrl_z(const kiv_hal::TRegisters &regs) {
	const kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	//LOGGER("entering ctrl z");
	const size_t buffer_size = 256;
	char buffer[buffer_size];
	size_t counter;

	do {
		if (kiv_os_rtl::Read_File(in, buffer, buffer_size, counter) == kiv_os::NOS_Error::Success && (counter > 0)) {
			if (counter == buffer_size) counter--;
			buffer[counter] = 0;
		
			if (counter == 0) {
				write.store(false);
				break;
			}
		}
		else
			break;	//EOF
	} while (write);

	write.store(false);
	//LOGGER("exiting ctrl z\n");
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;
}

size_t __stdcall gen_nums(const kiv_hal::TRegisters &regs) {
	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);
	//LOGGER("entering generator\n");
	size_t counter;

	const char* new_line = "\n";
	bool first = true;

	double num = .0;
	while (write) {
		if (!first) {
			kiv_os_rtl::Write_File(out_handle, new_line, strlen(new_line), counter);
		}

		first = false;
		num = ((double)rand() / (double)RAND_MAX);
		std::string string = std::to_string(num);
		const char* str = string.c_str();
		kiv_os_rtl::Write_File(out_handle, str, strlen(str), counter);
	}
	//LOGGER("exiting generator\n");
	kiv_os_rtl::Close_Handle(in_handle);
	kiv_os_rtl::Close_Handle(out_handle);
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;
}

size_t __stdcall rgen(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);
	//LOGGER("entering rgen\n");
	write.store(true);

	kiv_os_rtl::Signals_Register(handle_sigterm, kiv_os::NSignal_Id::Terminate);

	kiv_os::THandle handle1 = kiv_os_rtl::Create_Thread(static_cast<void *>(""), ctrl_z, in_handle, out_handle);
	kiv_os::THandle handle2 = kiv_os_rtl::Create_Thread(static_cast<void *>(""), gen_nums, in_handle, out_handle);
	std::vector<kiv_os::THandle> handles = { handle1, handle2 };

	kiv_os_rtl::Wait_For(handles);
	//LOGGER("exiting rgen\n");
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;	
}