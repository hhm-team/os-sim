#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include "sort.h"
#include "utils.h"
#include "rtl.h"

size_t __stdcall sort(const kiv_hal::TRegisters &regs) {
	const kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	std::string data = "";
	size_t read;
	const size_t buff_size = 1024;
	char buff[buff_size];
	do {
		kiv_os_rtl::Read_File(in_handle, buff, buff_size - 1, read);
		if (read > 0) {
			buff[read] = 0;
			data.append(buff);
		}
	} while (read > 0);

	std::vector<std::string> lines = std::vector<std::string>();
	std::stringstream stream(data);
	std::string segment;
	while (std::getline(stream, segment, '\n')) {
		if (segment.size() > 0) {
			lines.push_back(segment);
		}
	}

	kiv_os_rtl::Close_Handle(in_handle);
	std::sort(lines.begin(), lines.end());

	for (size_t i = 0; i < lines.size(); i++) {
		std::string line = lines[i];
		size_t count;
		kiv_os_rtl::Write_File(out_handle, line.c_str(), line.length(), count);
		if (i < lines.size() - 1) {
			kiv_os_rtl::Write_File(out_handle, "\n", 1, count);
		}
	}

	kiv_os_rtl::Close_Handle(out_handle);
	kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	return 0;
}