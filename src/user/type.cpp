#include "echo.h"
#include <thread>
#include <chrono>
#include <sstream>

#include "rtl.h"

size_t __stdcall type(const kiv_hal::TRegisters &regs) {

	kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	char* arg = reinterpret_cast<char*>(regs.rdi.r);
	if (strlen(arg) > 0) {
		// vypis souboru
		kiv_os_rtl::Open_File(arg, kiv_os::NOpen_File::fmOpen_Always, in_handle);
	}

	if (in_handle != kiv_os::Invalid_Handle) {
		const size_t buff_size = 1024;
		char buff[buff_size];
		size_t count = 0;
		do {
			kiv_os_rtl::Read_File(in_handle, buff, buff_size, count);
			if (count > 0) {
				size_t c;
				kiv_os_rtl::Write_File(out_handle, buff, count, c);
				count = c;
			}
		} while (count > 0);

		kiv_os_rtl::Close_Handle(in_handle);
		kiv_os_rtl::Close_Handle(out_handle);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
		return 0;	
	} else {
		kiv_os_rtl::Close_Handle(out_handle);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::File_Not_Found);
		return 0;
	}
}
