#include "freq.h"
#include "utils.h"
#include <sstream>
#include <iomanip>
#include <map>

#include "rtl.h"

size_t __stdcall ps(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);
	kiv_os::THandle file;

	kiv_os_rtl::Open_File(PROCFILE, kiv_os::NOpen_File::fmOpen_Always, file);

	if (file != kiv_os::Invalid_Handle) {
		const size_t buff_size = 1024;
		char buff[buff_size];
		size_t count = 0;
		size_t read_count;

		do {
			kiv_os_rtl::Read_File(file, buff, buff_size, count);

			read_count = count;
			if (count > 0) {
				kiv_os_rtl::Write_File(out_handle, buff, read_count, count);
			}
		} while (count != 0);

		kiv_os_rtl::Close_Handle(out_handle);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::Success);
	} else {
		kiv_os_rtl::Close_Handle(out_handle);
		kiv_os_rtl::Exit(kiv_os::NOS_Error::IO_Error);
	}

	return 0;
}