#include "process_management.h"
#include <memory>
#include <thread>
#include <algorithm>
#include <sstream>
#include <mutex>
#include <chrono>
#include <Windows.h>

std::mutex m_process;
std::recursive_mutex m_exit;
std::mutex m_procfile;

/*
Konstruktor Process_Management.

@param generator_ptr - pointer na instanci generatoru handlu
@param io_ptr - pointer na instanci io managementu
*/
Process_Management::Process_Management(std::shared_ptr<Handle_Generator> generator_ptr, std::shared_ptr<IO_Management> io_ptr) {
	process_table = {};
	thread_table = {};
	signals = std::make_shared<Signal_Management>();
	system_terminated = false;
	generator = generator_ptr;
	io_management = io_ptr;
}

Process_Management::~Process_Management() {
	thread_table.clear();
}

/*
Zjisti o jaky pozadavek tykajici se procesu se jedna
a preda jeho vyrizeni spravne funkci.

@param regs - registry vytvorene systemovym volanim
@param User_Programs - modul s ecportovanymi funkcemi
*/
void Process_Management::handle_process(kiv_hal::TRegisters & regs, HMODULE User_Programs)
{
	switch (static_cast<kiv_os::NOS_Process>(regs.rax.l)) {

	case kiv_os::NOS_Process::Clone:
		switch (static_cast<kiv_os::NClone>(regs.rcx.l)) {
		case kiv_os::NClone::Create_Process: {
			m_process.lock();

			kiv_os::THandle handle = create_process(regs, User_Programs);

			if (handle != kiv_os::Invalid_Handle) {
				regs.rax.x = static_cast<decltype(regs.rax.x)>(handle);
			}

			else {
				regs.flags.carry = 1;
				regs.rax.r = static_cast<decltype(regs.rax.r)>(kiv_os::NOS_Error::Unknown_Error);
			}
			m_process.unlock();
			break;
		}

		case kiv_os::NClone::Create_Thread:
			m_process.lock();

			kiv_os::THandle handle = create_thread(get_pid(), reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r), regs, true);
			regs.rax.x = static_cast<decltype(regs.rax.x)>(handle);

			m_process.unlock();
			break;
		}

		break;


	case kiv_os::NOS_Process::Register_Signal_Handler:

		signals->register_signal_handler(static_cast<kiv_os::NSignal_Id>(regs.rcx.l), reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r));
		break;

	case kiv_os::NOS_Process::Exit: {
		kiv_os::NOS_Error err = static_cast<kiv_os::NOS_Error>(regs.rcx.x);
		terminate(err, "", 0);
		break;
	}

	case kiv_os::NOS_Process::Read_Exit_Code: {
		read_exit_code(regs);
		break;
	}

	case kiv_os::NOS_Process::Shutdown: {

		signals->handle_signal(static_cast<uint8_t>(kiv_os::NSignal_Id::Terminate), regs);
		std::thread killer = std::thread(&Process_Management::thread_killer, this);
		killer.detach();
		break;
	}

	case kiv_os::NOS_Process::Wait_For:
		kiv_os::THandle handle = wait_for(regs);

		regs.rax.x = static_cast<decltype(regs.rax.x)>(handle);
		break;
	}
}

/*
Vraci id vlakna jako string

@return - id ve stringu
*/
std::string get_id() {
	std::ostringstream ss;
	ss << std::this_thread::get_id();
	return ss.str();
}

/*
Vytvori proces a vrati jeho handle

@param regs - registry vytvorene systemovym volanim
	rcx - kiv_os::NClone::Create_Process
	rdx - pointer na null - terminated string udavajici jmeno programu
	rdi - pointer na null-termined ANSI char string udavajici argumenty programu
	bx - 2x THandle na stdin a stdout, tj. bx.e = (stdin << 16) | stdout

@param User_Programs - modul s ecportovanymi funkcemi
@return - handle procesu, jinak kiv_os::Invalid_Handle
*/
kiv_os::THandle Process_Management::create_process(kiv_hal::TRegisters & regs, HMODULE User_Programs) {
	m_exit.lock();
	kiv_os::THandle handle = -1;
	char* program = reinterpret_cast<char*>(regs.rdx.r);
	kiv_hal::TRegisters new_regs;
	memset(&new_regs, 0, sizeof(kiv_hal::TRegisters));

	kiv_os::THandle in = (uint16_t)((regs.rbx.e & 0xFFFF0000) >> 16);
	kiv_os::THandle out = (uint16_t)(regs.rbx.e & 0x0000FFFF);

	kiv_os::TThread_Proc proc = (kiv_os::TThread_Proc)GetProcAddress(User_Programs, program);

	if (!proc) {
		m_exit.unlock();
		return kiv_os::Invalid_Handle;
	}

	new_regs.rax.x = static_cast<decltype(new_regs.rax.x)>(in);
	new_regs.rbx.x = static_cast<decltype(new_regs.rbx.x)>(out);
	new_regs.rdi.r = regs.rdi.r;

	auto pcb = std::make_shared<PCB>();
	pcb->name = program;
	pcb->pid = count++;
	pcb->exit_code = UINT16_MAX;
	pcb->threads = std::make_unique<std::vector<std::shared_ptr<TCB>>>();

	process_table.push_back(pcb);

	write_procfile();

	handle = create_thread(pcb->pid, proc, new_regs, false);

	m_exit.unlock();
	return handle;
}

/*
Kontrola zda na dane vlakno nekdo neceka. Pokud ano, je siganlizovan
semafor cekajiciho.

@param this_id - id vlakna
*/
void Process_Management::check_waiting(std::string this_id) {
	auto tcb = thread_table.find(this_id);

	if (tcb != thread_table.end()) {
		for (auto it = tcb->second->waiting_tids->begin(); it != tcb->second->waiting_tids->end(); ) {
			auto waiting_tcb = thread_table.find(*it);

			if (waiting_tcb != thread_table.end()) {
				if (waiting_tcb->second->semaphore != nullptr) {
					waiting_tcb->second->semaphore->up();
				}
			}

			++it;
		}
	}

	tcb->second->waiting_tids->clear();
}

/*
Ukonci vlakno. Pokud je vlakno primarni vlakno procesu,
je zapsan exit code.

@param err - exit code
*/
void Process_Management::terminate(kiv_os::NOS_Error err, std::string thread_id, size_t pid_) {
	std::string this_id;
	bool main = false;

	if (thread_id.empty()) {
		this_id = get_id();
	}

	else {
		this_id = thread_id;
	}

	m_exit.lock();
	size_t pid;

	if (thread_id.empty()) {
		pid = get_pid();
	}

	else {
		pid = pid_;
	}

	for (auto it = thread_table.begin(); it != thread_table.end(); ) {
		if (it->second->id == this_id) {
			//ukonceni vlakna
			it->second->state = Thread_State::Terminated;
			if (it->second->pid > 0) {
				it->second->thread->detach();
			}
			main = it->second->primary_thread;
			//kontrola zda na nej nekdo neceka
			check_waiting(this_id);

			break;
		}

		else {
			++it;
		}
	}

	if (main) {
		for (auto it = process_table.begin(); it != process_table.end(); ) {
			PCB & pcb = **it;

			if (pcb.pid == pid) {
				//zapis exit code
				pcb.exit_code = static_cast<uint16_t>(err);
				break;
			}

			else {
				++it;
			}
		}
	}
	m_exit.unlock();
}

/*
Vytvori vlakno a vrati jeho handle.

@param pid - process id proces, ktery vlastni vlakno
@param proc - funkce, kterou ma vlakno vykonavat
@param regs - registry vytvoreny pri volani systemoveho volani
@param create_thread_syscall - indikace zda se jedna primo o systemove volani vytvoreni vlakna nebo je tvoreno pri inicializaci procesu
@return - handle vlakna
*/
kiv_os::THandle Process_Management::create_thread(uint16_t pid, kiv_os::TThread_Proc proc, kiv_hal::TRegisters & regs, bool create_thread_syscall) {
	m_exit.lock();
	std::shared_ptr<std::thread> new_thread = nullptr;
	std::shared_ptr<TCB> tcb = std::make_shared<TCB>();
	kiv_os::THandle handle = -1;

	if (create_thread_syscall) {
		kiv_os::THandle in = (uint16_t)((regs.rbx.e & 0xFFFF0000) >> 16);
		kiv_os::THandle out = (uint16_t)(regs.rbx.e & 0x0000FFFF);
		regs.rax.x = static_cast<decltype(regs.rax.x)>(in);
		regs.rbx.x = static_cast<decltype(regs.rbx.x)>(out);
	}

	for (auto & pcb : process_table) {

		if (pcb->pid == pid) {

			new_thread = std::make_shared<std::thread>(proc, regs);

			auto id = new_thread->get_id();
			std::ostringstream ss;
			ss << new_thread->get_id();
			tcb->id = ss.str();
			tcb->pid = pid;
			tcb->thread = new_thread;
			tcb->state = Thread_State::Running;
			tcb->semaphore = nullptr;
			tcb->waiting_tids = std::make_unique<std::vector<std::string>>();
			tcb->handle = generator->get_handle();
			if (create_thread_syscall) {
				tcb->primary_thread = false;
			}

			else {
				tcb->primary_thread = true;
			}

			thread_table.insert({ tcb->id, tcb });

			pcb->threads->push_back(tcb);
			handle = tcb->handle;
			m_exit.unlock();
			return handle;
		}
	}
	m_exit.unlock();
	return handle;
}

/*
Cekani pred prectenim exit code - vsechna vlakna daneho procesu musi dobehnout.

@param pcb - pcb procesu, na ktery se ceka
*/
void Process_Management::wait_before_read_exit(PCB & pcb) {

	while (1) {

		std::vector<kiv_os::THandle> handles = {};
		size_t count = 0;
		//ulozeni vsech neukoncenych vlaken
		for (auto it = pcb.threads->begin(); it != pcb.threads->end(); ) {
			TCB & tcb = **it;

			if (tcb.state != Thread_State::Terminated) {
				handles.push_back(tcb.handle);
				count++;
			}

			++it;
		}

		if (handles.size() == 0) {
			return;
		}

		else {
			//volani wait_for
			kiv_os::THandle* arr = new kiv_os::THandle[count];
			std::copy(handles.begin(), handles.end(), arr);

			kiv_hal::TRegisters r;
			memset(&r, 0, sizeof(kiv_hal::TRegisters));
			r.rcx.r = static_cast<decltype(r.rcx.r)>(count);
			r.rdx.r = reinterpret_cast<decltype(r.rdx.r)>(arr);
			wait_for(r);
			delete[] arr;
		}

	}
}

/*
Cteni exit code procesu.

@param regs - registry vytvorene systemovym volanim
	IN	dx - handle procesu/thread jehoz exit code se ma cist
	OUT cx - exitcode
*/
void Process_Management::read_exit_code(kiv_hal::TRegisters & regs) {
	kiv_os::THandle handle = static_cast<kiv_os::THandle>(regs.rdx.x);

	uint16_t pid = get_pid(handle);

	for (auto it = process_table.begin(); it != process_table.end(); ) {
		PCB & pcb = **it;

		if (pcb.pid == pid) {
			//cekani na ukonceni vsech vlaken procesu
			wait_before_read_exit(pcb);
			break;
		}

		else {
			++it;
		}
	}

	for (auto it = process_table.begin(); it != process_table.end(); ) {
		PCB & pcb = **it;

		if (pcb.pid == pid) {

			m_exit.lock();
			regs.rcx.x = pcb.exit_code;

			//vycisteni zaznamu vlaken z thread table
			uint64_t erased_threads_count = 0;
			for (auto it2 = thread_table.begin(); it2 != thread_table.end(); ) {
				if (it2->second->pid == pid) {

					if (pid > 0) {
						if (it2->second->state != Thread_State::Terminated) {
							it2->second->thread->detach();
						}
					}
					generator->free_handle(it2->second->handle);

					it2 = thread_table.erase(it2);
					++erased_threads_count;

					if (erased_threads_count == pcb.threads->size()) {
						break;
					}
				}

				else {
					++it2;
				}
			}


			pcb.threads->clear();
			process_table.erase(it);
			m_exit.unlock();
			break;
		}

		else {
			++it;
		}
	}
}

/*
Cekani na prvni spusteny program, aby se kernel iihned neukoncil.
*/
void Process_Management::wait_for_first_program() {
	process_table[0]->threads->at(0)->thread->join();
}

/*
Ziskani pidu procesu.

@return - pid procesu
*/
uint16_t Process_Management::get_pid() {
	std::ostringstream ss;
	ss << std::this_thread::get_id();
	std::string tid = ss.str();
	for (auto it = thread_table.begin(); it != thread_table.end(); it++) {
		if (it->second->id == tid) {
			return it->second->pid;
		}
	}

	return UINT16_MAX;
}

/*
Zjisteni pidu procesu, kteremu patri dane vlakno.

@param handle - handle vlakna jehoz procesu je zjistovan pid
@return - pid procesu
*/
uint16_t Process_Management::get_pid(kiv_os::THandle handle) {
	for (auto it = thread_table.begin(); it != thread_table.end(); ) {
		if (it->second->handle == handle) {
			return it->second->pid;
		}

		else {
			++it;
		}
	}

	return -1;
}

/*--------------------------Wait_For--------------------------*/

/*
Pridani id cekajiciho vlakna do front cekajicich vlaken, na ktere se ceka.

@param arr - pole handlu vlaken na ktere se ma cekat
@param count - pocet vlaken
*/
void Process_Management::add_waiting_tid(kiv_os::THandle* arr, uint64_t count, std::string tid) {
	for (int i = 0; i < count; i++) {
		kiv_os::THandle handle = arr[i];

		for (auto it = thread_table.begin(); it != thread_table.end(); ) {
			if (it->second->handle == handle) {
				it->second->waiting_tids->push_back(tid);
				break;
			}

			else {
				++it;
			}
		}
	}
}

/*
Kontrola, zda nejake z vlaken, na ktere se ma cekat, jiz neskoncilo.

@param arr - pole vlaken na ktere se ma cekat
@param count - pocet vlaken v poli
@return - handle prvniho ukonceneho vlakna
*/
kiv_os::THandle Process_Management::check_if_allready_ended(kiv_os::THandle* arr, uint64_t count) {
	bool ended = false;
	kiv_os::THandle signalized = kiv_os::Invalid_Handle;

	for (int i = 0; i < count; i++) {
		kiv_os::THandle handle = arr[i];

		for (auto it = thread_table.begin(); it != thread_table.end(); ) {
			if (it->second->handle == handle) {
				if (it->second->state == Thread_State::Terminated) {
					ended = true;
					signalized = handle;

					if (ended) {
						return signalized;
					}
				}

				else {
					break;
				}
			}

			else {
				++it;
			}
		}
	}

	return signalized;
}

/*
Cekani na pole vlaken.

@param regs - registry vytvorene systemovym volanim
	rcx - kiv_os::NClone::Create_Thread
	rdx - kiv_os::TThread_Proc
	rdi - *data
@return - handle prvniho signalizovaneho vlakna
*/
kiv_os::THandle Process_Management::wait_for(kiv_hal::TRegisters & regs) {
	uint64_t count = static_cast<uint64_t>(regs.rcx.r);
	kiv_os::THandle* arr = reinterpret_cast<kiv_os::THandle*>(regs.rdx.r);

	bool ended = false;
	kiv_os::THandle signalized_handle = kiv_os::Invalid_Handle;
	Semaphore* semaphore = nullptr;

	auto tcb = thread_table.find(get_id());
	tcb->second->state = Thread_State::Blocked;

	m_exit.lock();
	//kontrola, zda jiz nekdo neskoncil
	signalized_handle = check_if_allready_ended(arr, count);
	if (signalized_handle != kiv_os::Invalid_Handle) {
		m_exit.unlock();
		tcb->second->state = Thread_State::Running;
		return signalized_handle;
	}

	else {
		//nikdo neskonci -> pridam do jejich fronty cekajicich id aktualniho vlakna
		add_waiting_tid(arr, count, get_id());
		semaphore = new Semaphore(1);
		tcb->second->semaphore = semaphore;
	}
	m_exit.unlock();

	semaphore->down();

	m_exit.lock();
	for (int i = 0; i < count; i++) {
		kiv_os::THandle handle = arr[i];

		for (auto it = thread_table.begin(); it != thread_table.end(); ) {
			if (it->second->handle == handle) {
				if (it->second->state == Thread_State::Terminated) {
					//nalezeno ukoncene vlakno - koncim cykly
					signalized_handle = handle;
					ended = true;
					break;
				}

				else {
					break;
				}
			}

			else {
				++it;
			}
		}

		if (ended) {
			break;
		}
	}
	m_exit.unlock();

	tcb->second->semaphore = nullptr;
	delete semaphore;
	tcb->second->state = Thread_State::Running;
	return signalized_handle;
}

/*
Vytvoreni procfile pro tasklist.
*/
void Process_Management::create_procfile() {
	kiv_hal::TRegisters regs_open;
	memset(&regs_open, 0, sizeof(kiv_hal::TRegisters));
	const char* file = ".procfile";
	regs_open.rdx.r = reinterpret_cast<decltype(regs_open.rdx.r)>(file);
	regs_open.rcx.l = static_cast<decltype(regs_open.rcx.l)>(kiv_os::NFile_Attributes::System_File);
	regs_open.rdi.r = static_cast<decltype(regs_open.rdi.r)>(0);
	io_management->open(regs_open);

	if (regs_open.flags.carry == 0) {
		procfile_handle = static_cast<kiv_os::THandle>(regs_open.rax.x);
	}
}

/*
Pretoceni procfile.
*/
void Process_Management::rewind_procfile() {
	// smazani dat
	kiv_hal::TRegisters regs_seek;
	memset(&regs_seek, 0, sizeof(kiv_hal::TRegisters));
	regs_seek.rdx.x = static_cast<decltype(regs_seek.rdx.x)>(procfile_handle);
	regs_seek.rdi.r = static_cast<decltype(regs_seek.rdi.r)>(0);
	regs_seek.rcx.l = static_cast<decltype(regs_seek.rcx.l)>(kiv_os::NFile_Seek::Set_Size);
	io_management->seek(regs_seek);
}

/*
Zavolani zapsani do procfile.
*/
void Process_Management::write_into_procfile(const char* str) {
	kiv_hal::TRegisters regs;
	memset(&regs, 0, sizeof(kiv_hal::TRegisters));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(procfile_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(str);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(strlen(str));

	io_management->read_write(regs, false);
}

/*
Zapis udaju do procfile.
*/
void Process_Management::write_procfile() {
	if (procfile_handle != kiv_os::Invalid_Handle) {
		m_exit.lock();
		std::stringstream str_stream;

		rewind_procfile();

		str_stream << "PID\t\t" << "Name" << std::endl;

		for (int i = 0; i < process_table.size(); i++) {
			std::shared_ptr<PCB> pcb = process_table[i];

			str_stream << pcb->pid << "\t\t" << pcb->name;
			if (i + 1 != process_table.size()) {
				str_stream << std::endl;
			}

			else {
				str_stream << '\0';
			}
		}

		auto x = str_stream.str();
		const char* str = x.c_str();

		write_into_procfile(str);

		m_exit.unlock();
	}
}

/*
Zabiti vlaken po dvou sekundach.
*/
void Process_Management::thread_killer() {
	std::this_thread::sleep_for(std::chrono::milliseconds(300));

	m_exit.lock();

	for (auto proc = process_table.begin(); proc != process_table.end(); proc++) {
		std::shared_ptr<PCB> pcb = *proc;
		if (pcb->pid > 0 && pcb->exit_code == UINT16_MAX && strcmp(pcb->name, "rgen") == 0) {

			for (auto thr = pcb->threads->begin(); thr != pcb->threads->end(); thr++) {
				std::shared_ptr<TCB> tcb = (*thr);

				if (tcb->state == Thread_State::Running) {
					if (tcb->semaphore != nullptr) {
						tcb->semaphore->up();
						delete tcb->semaphore;
						tcb->semaphore = nullptr;
					}

					terminate(kiv_os::NOS_Error::Success, tcb->id, tcb->pid);
					::TerminateThread(tcb->thread->native_handle(), 1);
				}
			}
		}
	}

	m_exit.unlock();
}

/*------------------------------------------------------------*/

PCB::PCB() { }

PCB::~PCB() { }
