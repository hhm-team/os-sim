#include "io_management.h"

IO_Management::IO_Management(std::shared_ptr<Handle_Generator> ptr, std::shared_ptr<File_System> fs_ptr) {
	file_desc_management = std::make_shared<File_Desc_Management>(ptr, fs_ptr);
}

IO_Management::~IO_Management() {
}

void IO_Management::handle_io(kiv_hal::TRegisters &regs)
{
	switch (static_cast<kiv_os::NOS_File_System>(regs.rax.l))
	{
		case kiv_os::NOS_File_System::Open_File: open(regs); break;
		case kiv_os::NOS_File_System::Write_File: read_write(regs, false); break;
		case kiv_os::NOS_File_System::Read_File: read_write(regs, true); break;
		case kiv_os::NOS_File_System::Seek: seek(regs); break;
		case kiv_os::NOS_File_System::Close_Handle: close(regs); break;
		case kiv_os::NOS_File_System::Delete_File: remove(regs); break;
		case kiv_os::NOS_File_System::Set_Working_Dir: set_dir(regs); break;
		case kiv_os::NOS_File_System::Get_Working_Dir: get_dir(regs); break;
		case kiv_os::NOS_File_System::Create_Pipe: create_pipe(regs); break;
	}
}

void IO_Management::open(kiv_hal::TRegisters &regs)
{
	const char *file_name = reinterpret_cast<const char *>(regs.rdx.r);
	kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(regs.rcx.l);
	kiv_os::NFile_Attributes attributes = static_cast<kiv_os::NFile_Attributes>(regs.rdi.r);

	kiv_os::NOS_Error result;
	kiv_os::THandle result_handle = file_desc_management->open_file(file_name, flags, attributes, result);

	if (result == kiv_os::NOS_Error::Success)
	{
		regs.rax.x = static_cast<decltype(regs.rax.x)>(result_handle);
	}
	else
	{
		set_error(regs, result);
	}
}

void IO_Management::read_write(kiv_hal::TRegisters &regs, bool read)
{
	Descriptor *desc = file_desc_management->find_file_desc(static_cast<kiv_os::THandle>(regs.rdx.x));
	if (desc == nullptr)
	{
		set_error(regs, kiv_os::NOS_Error::IO_Error);
	}
	else if (read)
	{
		desc->read(regs);
	}
	else
	{
		desc->write(regs);
	}
}

void IO_Management::seek(kiv_hal::TRegisters &regs)
{
	kiv_os::THandle handle = static_cast<kiv_os::THandle>(regs.rdx.x);
	size_t offset = static_cast<size_t>(regs.rdi.r);
	kiv_os::NFile_Seek attribute = static_cast<kiv_os::NFile_Seek>(regs.rcx.l);

	kiv_os::NOS_Error result;
	size_t position = 0;

	Descriptor *desc = file_desc_management->find_file_desc(static_cast<kiv_os::THandle>(regs.rdx.x));
	if (desc == nullptr)
	{
		result = kiv_os::NOS_Error::IO_Error;
	}
	else
	{
		FileIO *file = dynamic_cast<FileIO *>(desc);
		if (file)
		{
			// Instance FileIO. Pouze u ni vykonavat seek.
			position = file->seek(handle, offset, attribute, result);
		}
		else
		{
			result = kiv_os::NOS_Error::Permission_Denied;
		}
	}

	if (result == kiv_os::NOS_Error::Success)
	{
		if (attribute == kiv_os::NFile_Seek::Get_Position)
		{
			regs.rax.r = static_cast<decltype(regs.rax.r)>(position);
		}
	}
	else
	{
		set_error(regs, result);
	}
}

void IO_Management::close(kiv_hal::TRegisters &regs)
{
	kiv_os::THandle handle = static_cast<kiv_os::THandle>(regs.rdx.x);
	if (!file_desc_management->close_handle(handle))
	{
		set_error(regs, kiv_os::NOS_Error::IO_Error);
	}
}

void IO_Management::remove(kiv_hal::TRegisters &regs)
{
	const char *file_name = reinterpret_cast<const char *>(regs.rdx.r);
	
	kiv_os::NOS_Error result;
	file_desc_management->remove_file(file_name, false, result);

	if (result != kiv_os::NOS_Error::Success)
	{
		set_error(regs, result);
	}
}

void IO_Management::remove_any_file(kiv_hal::TRegisters &regs)
{
	const char *file_name = reinterpret_cast<const char *>(regs.rdx.r);

	kiv_os::NOS_Error result;
	file_desc_management->remove_file(file_name, true, result);

	if (result != kiv_os::NOS_Error::Success)
	{
		set_error(regs, result);
	}
}

void IO_Management::set_dir(kiv_hal::TRegisters &regs)
{
	const char *dir_path = reinterpret_cast<const char *>(regs.rdx.r);
	
	kiv_os::NOS_Error result;
	file_desc_management->set_working_dir(dir_path, result);

	if (result != kiv_os::NOS_Error::Success)
	{
		set_error(regs, result);
	}
}

void IO_Management::get_dir(kiv_hal::TRegisters &regs)
{
	char *buffer = reinterpret_cast<char *>(regs.rdx.r);
	size_t buffer_size = static_cast<size_t>(regs.rcx.r);
	
	MftItem *item = file_desc_management->get_working_dir();
	
	if (!item || buffer_size == 12)
	{
		// Ulozit nazev pracovniho adresare.
		if (!item)
		{
			memset(buffer, '\0', buffer_size);
		}
		else
		{
			memcpy(buffer, item->item_name, buffer_size);
		}
	}
	else
	{
		// Vytvoreni cesty k pracovnimu adresari (zacatek radky v shellu).

		std::string x;
		x += ">";
		int32_t id = item->uid;

		bool first = true;
		while (true)
		{
			MftItem *_item = file_desc_management->get_mft_item(id);
			if (_item->parent_uid != 0)
			{
				if (first)
				{
					first = false;
				}
				else
				{
					x.insert(0, "\\");
				}
				x.insert(0, _item->item_name);
				id = _item->parent_uid;
			}
			else
			{
				break;
			}
		}

		x.insert(0, "C:\\");
		strcpy_s(buffer, buffer_size, x.c_str());
	}
	
	regs.rax.r = static_cast<decltype(regs.rax.r)>(buffer_size);
}

void IO_Management::create_pipe(kiv_hal::TRegisters &regs)
{
	kiv_os::THandle *array = reinterpret_cast<kiv_os::THandle *>(regs.rdx.r);
	kiv_os::NOS_Error result;
	file_desc_management->create_pipe(array, result);

	if (result != kiv_os::NOS_Error::Success)
	{
		set_error(regs, result);
	}
}

void IO_Management::set_error(kiv_hal::TRegisters &regs, kiv_os::NOS_Error error)
{
	regs.flags.carry = true;
	regs.rax.r = static_cast<decltype(regs.rax.r)>(error);
}