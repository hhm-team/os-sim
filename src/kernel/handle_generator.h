#pragma once

#include "..\api\api.h"
#include <random>
#include <set>
#include <mutex>

// Generator handelu.
class Handle_Generator {
private:
	std::random_device rd;
	std::mt19937 gen;
	std::uniform_int_distribution<> dis;

	std::set<kiv_os::THandle> handles;
	std::mutex handles_mutex;

public:
	Handle_Generator();

	// Vygeneruje nahodny handle z celeho mozneho rozsahu.
	kiv_os::THandle get_handle();

	// Uvolni handle.
	bool free_handle(kiv_os::THandle handle);
};