#pragma once

#include <windows.h>
#include <map>
#include <memory>
#include "../api/api.h"
#include "handle_generator.h"
#include "ntfs/ntfs.h"
#include "file_system_api.h"
#include "semaphore.h"

// Rodic pro jednotlive typy IO. Jsou tri - STD, FILE a PIPE.
class Descriptor {
public:
	// Handle souboru.
	kiv_os::THandle handle;

	// Nazev souboru.
	const char* file;

	// Prislusny mft_item z NTFS.
	MftItem* mft_item;

	// Mutex pro osetreni pristupu k funkcim read a write.
	std::mutex mutex;

	// Potomci musi prekryt funkce write() a read(), pro zapis a cteni.
	virtual void write(kiv_hal::TRegisters& regs) = 0;
	virtual void read(kiv_hal::TRegisters& regs) = 0;
};

// Predstavuje konzoli.
class StdIO : public Descriptor {
public:
	StdIO(kiv_os::THandle handle, kiv_os::NFile_Attributes permission, const char* file, MftItem* mft_item);
	virtual void write(kiv_hal::TRegisters& regs) override;
	virtual void read(kiv_hal::TRegisters& regs) override;
private:
	// Opravneni k pristupu.
	kiv_os::NFile_Attributes permission;
};

// Predstavuje soubor v souborovem systemu.
class FileIO : public Descriptor {
public:
	FileIO(std::shared_ptr<File_System> fs, kiv_os::THandle handle, const char* file, MftItem* mft_item);
	~FileIO();
	virtual void write(kiv_hal::TRegisters& regs) override;
	virtual void read(kiv_hal::TRegisters& regs) override;

	// Navic funkce seek, ktera nastavuje offset.
	size_t seek(kiv_os::THandle handle, size_t offset, kiv_os::NFile_Seek attribute, kiv_os::NOS_Error &result);

private:
	// Pointer na souborovy system.
	std::shared_ptr<File_System> fs;

	// V pripade, ze soubor je adresar, pri otevreni se vytvori pole prvku TDir_Entry, viz api.h.
	kiv_os::TDir_Entry *entries;

	// Pocet prvku TDir_Entry v poli entries.
	size_t entries_count;

	// Offset v souboru. Upravovano ve vsech trech funkcich (read, write a seek).
	int32_t offset;

	// Funkce pro vytvoreni a naplneni pole entries prvky TDir_Entry.
	void create_TDirEntries();
};

// Predstavuje rouru.
class PipeIO: public Descriptor
{
	public:
		explicit PipeIO(kiv_os::THandle write, kiv_os::THandle read);
		~PipeIO();

		// @return jestli ma byt objekt roury vymazan z registru otevrenych souboru.
		bool closing(kiv_os::THandle handle);

		virtual void write(kiv_hal::TRegisters &regs) override;
		virtual void read(kiv_hal::TRegisters &regs) override;

	private:
		// Celkova velikost bufferu (neda se prekrocit).
		static const int BUFFER_SIZE;

		// Buffer pro data.
		char *buffer;

		// Aktualni velikost dat v bufferu.
		size_t act_size;

		// Semafory pro producent-konzument.
		Semaphore s_empty;
		Semaphore s_full;
		Semaphore s_mutex;

		// Handel pro proces, ktery zapisuje.
		kiv_os::THandle write_handle;
		// Handel pro proces, ktery cte.
		kiv_os::THandle read_handle;
		
		// Jestli zapisovat ukoncil zapisovani.
		bool writer_closed;
};

class File_Desc_Management {
private:
	// Tabulka otevrenych souboru.
	std::map<uint16_t, Descriptor*> table;

	// Generator handelu.
	std::shared_ptr<Handle_Generator> generator;

	// Souborovy system.
	std::shared_ptr<File_System> disk_management;

	// Pracovni adresar.
	MftItem *working_dir;

	// Mutex pro osetreni pristupu k funkcim, ktere pracuji s globalnimi atributy.
	std::mutex mutex;

public:
	File_Desc_Management(std::shared_ptr<Handle_Generator> ptr, std::shared_ptr<File_System> fs_ptr);
	~File_Desc_Management();

	// Zkusi najit otevreny soubor s danym handlem.
	Descriptor* find_file_desc(kiv_os::THandle handle);
	
	// Funkce vykonavaji cinnosti systemovych volani souboroveho systemu, viz api.h.
	kiv_os::THandle open_file(const char *file, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, kiv_os::NOS_Error &result);
	bool close_handle(kiv_os::THandle handle);
	void remove_file(const char *dir_path, bool any, kiv_os::NOS_Error &result);
	void set_working_dir(const char *dir_path, kiv_os::NOS_Error &result);
	MftItem *get_working_dir();
	MftItem *get_mft_item(int32_t id);
	void create_pipe(kiv_os::THandle *array, kiv_os::NOS_Error &result);
};