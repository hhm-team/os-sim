#include "handle_generator.h"

Handle_Generator::Handle_Generator(): gen(rd()), dis(3, UINT16_MAX)
{
}

kiv_os::THandle Handle_Generator::get_handle() {
	std::lock_guard<std::mutex> guard(handles_mutex);
	while (true)
	{
		// Dokud nebude vygenerovan handle, ktery neni v seznamu.
		kiv_os::THandle newHandle = static_cast<kiv_os::THandle>(dis(gen));
		if (handles.find(newHandle) == handles.end())
		{
			handles.insert(newHandle);
			return newHandle;
		}
	}
}

bool Handle_Generator::free_handle(kiv_os::THandle handle)
{
	std::lock_guard<std::mutex> guard(handles_mutex);
	bool exists = false;

	std::set<kiv_os::THandle>::iterator position = handles.find(handle);
	if (position != handles.end())
	{
		exists = true;
		handles.erase(position);
	}

	return exists;
}