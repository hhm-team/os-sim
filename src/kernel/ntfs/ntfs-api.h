#pragma once

#include <mutex>
#include "..\file_system_api.h"
#include "..\..\api\hal.h"
#include "..\..\api\api.h"

extern "C" {
#include "ntfs.h"
}

class File_System_NTFS : public File_System {
private:
	std::recursive_mutex mutex;

	uint8_t disk_drive_number;
	kiv_hal::TDrive_Parameters drive_parameters;

	NTFS* ntfs;
	MftItem* root_dir;
	MftItem* debug_working_dir;

	void Clear_Error_Code();
	kiv_os::NOS_Error Get_Error_Code();

public:
	File_System_NTFS(uint8_t disk_drive_number, kiv_hal::TDrive_Parameters drive_parameters);

	void Run_Debug_Command(const char* command) override;
	MftItem* Get_Root_Dir() override;
	MftItem* Find_File(const char *path, MftItem *working_dir) override;
	MftItem* Find_File(int32_t uid) override;
	MftItem* Create_File(const char *name, MftItem *dir, uint8_t attributes, kiv_os::NOS_Error *out_error_code) override;
	void Remove_File(MftItem *file, kiv_os::NOS_Error *out_error_code) override;
	int32_t Resize_File(MftItem *file, int32_t new_size_in_bytes, kiv_os::NOS_Error *out_error_code) override;
	int32_t Write(MftItem *file, int32_t offset_in_bytes, const void* data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) override;
	int32_t Read(MftItem *file, int32_t offset_in_bytes, void* data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) override;
	uint32_t Get_Total_Size() override;
	void Close() override;
};
