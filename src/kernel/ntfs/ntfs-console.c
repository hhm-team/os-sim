
/**
 * Implementuje funkce CLI.
 *
 * @author: Patrik Harag
 * @version: 2018-11-14
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "ntfs-console.h"
#include "ntfs-console-commands.h"
#include "ntfs-io.h"
#include "ntfs-module-find.h"
#include "ntfs-module-allocation.h"
#include "ntfs-debug.h"


bool REPL_TEST = false;

void console_state_init(ConsoleState *console_state) {
	memset(console_state, 0, sizeof(ConsoleState));
}

void console_state_free(ConsoleState *console_state) {

}

static Result console_eval(ConsoleState* console_state, char** input, int count) {
	assert(count > 0);

	NTFS* ntfs = console_state->ntfs;
	char* command = input[0];

	// příkazy, které je možné provést bez inicializovaného ntfs

	if (strcmp(command, "init") == 0) {
		return command_init(console_state, input, count);

	} else if (strcmp(command, "load") == 0) {
		// Načte soubor z pevného disku, ve kterém budou jednotlivé příkazy a
		// začne je sekvenčně vykonávat. Formát je 1 příkaz/1řádek
		// load s1

		assert(count == 2);
		return console_eval_script(console_state, input[1]);

	} else if (strcmp(command, "disable") == 0) {
		assert(count == 2);

		char* sub_command = input[1];

		if (strcmp(sub_command, "first-fit") == 0) {
			DISABLE_FIRST_FIT = true;
			ntfs_debug("OK\n");
			return RESULT_OK;

		} else {
			ntfs_debug("Unknown parameter\n");
			return RESULT_WARNING;
		}
	}

	if (ntfs->initialized == false) {
		ntfs_debug("File system is not initialized\n");
		ntfs_debug("Command: init <size in bytes>\n");
		return RESULT_WARNING;
	}

	// příkazy, které je možné provést pouze s inicializovaným ntfs

	if (strcmp(command, "show") == 0) {
		assert(count == 2);

		char* sub_command = input[1];

		if (strcmp(sub_command, "parameters") == 0) {
			return command_show_parameters(ntfs);

		} else if (strcmp(sub_command, "bitmap") == 0) {
			return command_show_bitmap(ntfs);

		} else if (strcmp(sub_command, "mft") == 0) {
			return command_show_mft(ntfs);

		} else {
			ntfs_debug("Unknown parameter\n");
			return RESULT_WARNING;
		}

	} else if (strcmp(command, "df") == 0) {
		return command_df(ntfs);

	} else if (strcmp(command, "info") == 0) {
		// Vypíše informace o souboru/adresáři s1/a1
		// (v jakých fragmentech/clusterech se nachází), uid, ...
		// info s1/a1

		assert(count == 2);
		return command_info(console_state, input[1]);

	} else if (strcmp(command, "pwd") == 0) {
		return command_pwd(console_state);

	} else if (strcmp(command, "cd") == 0) {
		assert(count == 2);
		return command_cd(console_state, input[1]);

	} else if (strcmp(command, "ls") == 0) {
		assert(count == 1 || count == 2);
		return command_ls(console_state, (count == 1) ? "" : input[1]);

	} else if (strcmp(command, "mkdir") == 0) {
		assert(count == 2);
		return command_mkdir(console_state, input[1]);

	} else if (strcmp(command, "rmdir") == 0) {
		assert(count == 2);
		return command_rmdir(console_state, input[1]);

	} else if (strcmp(command, "create") == 0) {
		assert(count == 2);
		return command_create(console_state, input[1]);

	} else if (strcmp(command, "rm") == 0) {
		// Smaže soubor s1
		// rm s1

		assert(count == 2);
		return command_rm(console_state, input[1]);

	} else if (strcmp(command, "read") == 0) {
		// Vypíše obsah souboru s1 od offsetu a, max b bajtů
		// read s1 [a b]

		assert(count == 2 || count == 4);
		if (count == 2) {
			return command_read(console_state, input[1], 0, -1);
		} else {
			return command_read(console_state, input[1], utils_parse_int(input[2]), utils_parse_int(input[3]));
		}

	} else if (strcmp(command, "write") == 0) {
		// Zapíše do souboru s1 od offsetu a řetězec b
		// write s1 [a] b

		assert(count == 3 || count == 4);
		if (count == 3) {
			return command_write(console_state, input[1], -1, input[2], (int32_t) strlen(input[2]));
		} else {
			return command_write(console_state, input[1], utils_parse_int(input[2]), input[3], (int32_t) strlen(input[3]));
		}

	} else if (strcmp(command, "resize") == 0) {
		assert(count == 3);
		return command_resize(console_state, input[1], utils_parse_int(input[2]));

	} else if (strcmp(command, "incp") == 0) {
		// Nahraje soubor s1 z pevného disku do umístění s2 v pseudoNTFS
		// incp s1 s2

		assert(count == 3);
		return command_incp(console_state, input[1], input[2]);

	} else if (strcmp(command, "exit") == 0 || strcmp(command, "quit") == 0) {
		return RESULT_EXIT;

	} else {
		ntfs_debug("UNKNOWN COMMAND: '%s'\n", command);
	}

	return RESULT_OK;
}

Result console_eval_command(ConsoleState* console_state, const char* input) {
	int argc;
	char** argv = utils_split_by_space(input, &argc);

	Result r = console_eval(console_state, argv, argc);

	for (size_t i = 0; i < argc; i++) {
		free(argv[i]);
	}
	free(argv);

	return r;
}

Result console_eval_script(ConsoleState* console_state, char* script_file) {
	FILE *file;
	fopen_s(&file, script_file, "r");
	if (file == NULL) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	Result exit_status = RESULT_OK;
	char buffer[128];  // max délka řádky
	while (fgets(buffer, sizeof(buffer), file) != NULL) {
		// odstanění new-line
		size_t len = strlen(buffer);
		if (buffer[len - 1] == '\n')
			buffer[len - 1] = '\0';

		exit_status = console_eval_command(console_state, buffer);

		if (!(exit_status == RESULT_OK || exit_status == RESULT_WARNING))
			break;
	}
	fclose(file);

	return exit_status;
}
