#include "cpp-bridge.h"
#include "../kernel.h"
#include "../handles.h"

#ifdef __cplusplus
extern "C" {
#endif

static bool io_proxy_handle_result(NTFS *ntfs, kiv_hal::TRegisters regs) {
	if (regs.flags.carry != 0) {
		// error
		auto code = static_cast<kiv_hal::NDisk_Status>(regs.rax.x);
		if (code != kiv_hal::No_Error) {
			ntfs->disk_last_error = static_cast<uint16_t>(code);
			return false;
		} else {
			return true;  // to by se asi st�vat nem�lo...
		}
	}
	return true;
}

bool cpp_bridge_write_sectors(NTFS *ntfs, int32_t offset, void *data, int32_t number_of_sectors) {
	kiv_hal::TDisk_Address_Packet address;
	address.lba_index = offset;
	address.sectors = data;
	address.count = number_of_sectors;

	kiv_hal::TRegisters regs;
	regs.rdx.l = ntfs->disk_number;  // na jak� disk
	regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Write_Sectors);  // operace
	regs.rdi.r = reinterpret_cast<uint64_t>(&address);  // adresa + pointer na data co se maj� zapsat

	kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
	return io_proxy_handle_result(ntfs, regs);
}

bool cpp_bridge_read_sectors(NTFS *ntfs, int32_t offset, void *data, int32_t number_of_sectors) {
	kiv_hal::TDisk_Address_Packet address;
	address.lba_index = offset;
	address.sectors = data;
	address.count = number_of_sectors;

	kiv_hal::TRegisters regs;
	regs.rdx.l = ntfs->disk_number;  // na jak� disk
	regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Read_Sectors);  // operace
	regs.rdi.r = reinterpret_cast<uint64_t>(&address);  // adresa + pointer na data co se maj� zapsat

	kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
	return io_proxy_handle_result(ntfs, regs);
}

void cpp_bridge_print(char* str) {
	kiv_hal::TRegisters registers;
	registers.rax.h = static_cast<decltype(registers.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
	registers.rdx.r = reinterpret_cast<decltype(registers.rdx.r)>(str);
	registers.rcx.r = strlen(str);

	kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);
}

#ifdef __cplusplus
}
#endif
