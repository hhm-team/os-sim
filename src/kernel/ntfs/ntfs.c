
/**
 * Implementuje základní funkce pro práci s ntfs.
 *
 * @author: Patrik Harag
 * @version: 2018-11-14
 */

#include <stdlib.h>
#include <assert.h>
#include "ntfs.h"
#include "ntfs-io.h"
#include "ntfs-module-allocation.h"
#include "ntfs-module-utils.h"
#include "ntfs-module-find.h"
#include "ntfs-debug.h"
#include "cpp-bridge.h"


NTFS* ntfs_create(uint8_t disk_number, int32_t disk_sector_size) {
	NTFS* ntfs = (NTFS*) utils_calloc(1, sizeof(NTFS));
	ntfs->disk_number = disk_number;
	ntfs->disk_sector_size = disk_sector_size;
	ntfs->disk_last_error = 0;
	ntfs->initialized = false;
	ntfs->boot_record = (BootRecord*) calloc(1, sizeof(BootRecord));
	ntfs->mft = (Mft*) calloc(1, sizeof(Mft));
	ntfs->bitmap = (Bitmap*) calloc(1, sizeof(Bitmap));
	return ntfs;
}

void ntfs_free(NTFS* ntfs) {
	ntfs_boot_record_free(ntfs->boot_record);
	free(ntfs->boot_record);

	ntfs_mft_free(ntfs->mft);
	free(ntfs->mft);

	ntfs_bitmap_free(ntfs->bitmap);
	free(ntfs->bitmap);
}

MftItem* ntfs_init(NTFS* ntfs, int32_t disk_size, int32_t cluster_size, int32_t mft_size) {
	if (ntfs->initialized) {
		ntfs_boot_record_free(ntfs->boot_record);
		ntfs_mft_free(ntfs->mft);
		ntfs_bitmap_free(ntfs->bitmap);

		ntfs->initialized = false;
	}

	ntfs_boot_record_init(ntfs->boot_record, disk_size, cluster_size, mft_size);
	ntfs_mft_init(ntfs->mft, mft_size);
	ntfs_bitmap_init(ntfs->bitmap, ntfs->boot_record->cluster_count);
	ntfs->initialized = true;

	MftItem* root = ntfs_create_root_dir(ntfs);
	if (root != NULL && io_disk_format(ntfs)) {
		return root;
	}
	return NULL;
}

MftItem* ntfs_init_disk_of_total_size(NTFS* ntfs, int32_t total_size_bytes, int32_t cluster_size) {
	int32_t mft_item_count = ntfs_mft_count_size(total_size_bytes);

	int32_t max_size_of_data_and_bitmap = total_size_bytes
		- sizeof(BootRecord)
		- mft_item_count * sizeof(MftItem);

	int32_t INCREMENT = 4 * 8;
	// 4 je počet bajtů v int32_t - chceme to mít zarovnané
	int32_t clusters = INCREMENT;
	while (((clusters + INCREMENT) * cluster_size) + ((clusters + INCREMENT) / INCREMENT) <= max_size_of_data_and_bitmap) {
		clusters += INCREMENT;
	}

	int32_t disk_size_bytes = clusters * cluster_size;

	// TODO: nedostatek místa

	// vytvoříme nový
	return ntfs_init(ntfs, disk_size_bytes, cluster_size, mft_item_count);
}

MftItem* ntfs_load_or_init_disk_of_total_size(NTFS* ntfs, int32_t total_size_bytes, int32_t cluster_size) {
	if (io_disk_try_load(ntfs)) {
		// načteno
		return ntfs_find_root(ntfs);
	} else {
		return ntfs_init_disk_of_total_size(ntfs, total_size_bytes, cluster_size);
	}
}

MftItem* ntfs_create_root_dir(NTFS* ntfs) {
	assert(ntfs->initialized);

	MftItem* dir = ntfs_create_empty_file(ntfs, "/", NULL, NTFS_ATTRIBUTE_DIRECTORY);
	assert(dir != NULL);
	return dir;
}

bool ntfs_add_item_into_dir(NTFS *ntfs, MftItem *dir, int32_t uid) {
	assert(dir != NULL && dir->attributes & NTFS_ATTRIBUTE_DIRECTORY);
	assert(dir->item_size % sizeof(int32_t) == 0);

	int32_t size_before = dir->item_size;
	if (io_file_write_append(ntfs, dir, &uid, 4) != 4) {
		ntfs_trim_file(ntfs, dir, size_before);
		if (ntfs->disk_last_error == NTFS_ERROR_CODE_SUCCESS) {
			ntfs->disk_last_error = NTFS_ERROR_CODE_NOT_ENOUGH_DISK_SPACE;
		}
		return false;
	}
	return true;
}

Result ntfs_remove_item_from_dir(NTFS *ntfs, MftItem *dir, int32_t uid) {
	assert(dir != NULL && dir->attributes & NTFS_ATTRIBUTE_DIRECTORY);
	assert(dir->item_size % sizeof(int32_t) == 0);

	// načtení seznamu souborů
	char *data = (char*) utils_malloc((dir->item_size + 1) * sizeof(char));
	if (io_file_read(ntfs, dir, 0, data, dir->item_size) != dir->item_size) {
		free(data);
		return RESULT_ERROR;
	}

	// odstranění položky
	int32_t new_data_size = dir->item_size - sizeof(int32_t);
	char *new_data = (char*) utils_malloc((new_data_size) * sizeof(char));
	int32_t entries = dir->item_size / sizeof(int32_t);

	int i = 0, j = 0;
	for (; i < entries; ++i) {
		int32_t next_uid = ((int32_t*) data)[i];

		if (next_uid != uid) {
			((int32_t*) new_data)[j++] = next_uid;
		}
	}

	free(data);

	if (i == j) {
		ntfs_debug("Item %d not found\n", uid);
		free(new_data);
		return RESULT_OK;
	} else if (i - j > 1) {
		ntfs_debug("Item %d was present multiple times\n", uid);
	}

	// smazání starých dat
	ntfs_clean_content(ntfs, dir);

	// zápis nových dat
	int32_t new_size = j * sizeof(int32_t);
	if (io_file_write_append(ntfs, dir, new_data, new_size) != new_size) {
		if (ntfs->disk_last_error == NTFS_ERROR_CODE_SUCCESS) {
			ntfs->disk_last_error = NTFS_ERROR_CODE_NOT_ENOUGH_DISK_SPACE;
		}
		free(new_data);
		return RESULT_ERROR;
	}
	free(new_data);
	return RESULT_OK;
}

MftItem* ntfs_create_empty_file(NTFS* ntfs, const char* name, MftItem* dir, uint8_t attributes) {
	int32_t uid = ntfs_mft_generate_random_uid(ntfs->mft);

	// alokace mft položky
	MftItem *item = NULL;
	if (ntfs_mft_find_free_mft_items(ntfs->mft, 0, 1, &item) != 1) {
		ntfs_debug("NOT ENOUGH MFT ITEMS\n");
		ntfs->disk_last_error = NTFS_ERROR_CODE_NOT_ENOUGH_DISK_SPACE;
		return NULL;
	}

	if (dir != NULL) {
		// přidání souboru do složky - může také selhat na nedostatek clusterů
		if (!ntfs_add_item_into_dir(ntfs, dir, uid)) {
			return NULL;
		}
	} else {
		assert(attributes & NTFS_ATTRIBUTE_DIRECTORY && strcmp(name, "/") == 0);  // root
	}

	// nastavení mft položky
	ntfs_mft_init_mft_item(item);
	item->uid = uid;
	item->parent_uid = (dir != NULL) ? dir->uid : NTFS_MFT_UID_ITEM_FREE;
	size_t strncpy_len = utils_min(NTFS_MFT_ITEM_NAME_LENGTH - 1, strlen(name));
	memcpy(item->item_name, name, strncpy_len);
	item->attributes = attributes;
	item->item_size = 0;
	item->item_order = 0;
	item->item_total = 1;
	return item;
}

MftItem* ntfs_create_file(NTFS* ntfs, char* name, void* data, int32_t size_bytes, MftItem* dir) {
	assert(dir != NULL);

	MftItem* first_item = ntfs_create_empty_file(ntfs, name, dir, 0);
	if (first_item == NULL) {
		return NULL;
	}

	if (io_file_write_append(ntfs, first_item, data, size_bytes) != size_bytes) {
		ntfs_remove(ntfs, first_item, dir);
		if (ntfs->disk_last_error == NTFS_ERROR_CODE_SUCCESS) {
			ntfs->disk_last_error = NTFS_ERROR_CODE_NOT_ENOUGH_DISK_SPACE;
		}
		return NULL;
	}
	return first_item;
}

void ntfs_remove(NTFS* ntfs, MftItem* first_item, MftItem* dir) {
	// odstranění položky z adresáře
	ntfs_remove_item_from_dir(ntfs, dir, first_item->uid);

	// smazání obsahu
	ntfs_clean_content(ntfs, first_item);

	// vyčištění první MFT položky
	ntfs_clean_mft_item(ntfs, first_item, true, true);
}

void ntfs_clean_content(NTFS* ntfs, MftItem* first_item) {
	ntfs_trim_file(ntfs, first_item, 0);
}

void ntfs_clean_mft_item(NTFS* ntfs, MftItem* item, bool full, bool change_bitmap) {
	// smazání obsahu - uvolnění clusterů
	for (int i = 0; i < NTFS_MFT_FRAGMENTS_COUNT; ++i) {
		MftFragment* fragment = &(item->fragments[i]);
		if (ntfs_mft_fragment_is_empty(fragment))
			break;

		if (change_bitmap) {
			ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, false);
		}
		fragment->start_index = 0;
		fragment->count = 0;
	}

	if (full) {
		// smazání všech hodnot
		ntfs_mft_init_mft_item(item);
	}
}
