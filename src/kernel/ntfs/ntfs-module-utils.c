
/**
 * Implementuje různé pomocné funkce pro práci s ntfs.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#include <assert.h>
#include <stdlib.h>
#include "ntfs-module-utils.h"
#include "ntfs-module-find.h"
#include "ntfs-debug.h"


int32_t ntfs_count_clusters(NTFS* ntfs, MftItem* item) {
	return utils_div_ceil(item->item_size, ntfs->boot_record->cluster_size);
}

int32_t ntfs_count_clusters_real(NTFS* ntfs, MftItem* first_item) {
	int32_t mft_item_count = first_item->item_total;
	MftItem **mft_items = (MftItem**) utils_malloc((mft_item_count) * sizeof(MftItem*));
	ntfs_collect_mft_items(ntfs, first_item, -1, mft_items);

	int32_t count = 0;
	for (int i = 0; i < mft_item_count; ++i) {
		MftItem* item = mft_items[i];
		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &item->fragments[j];
			count += fragment->count;
		}
	}
	free(mft_items);
	return count;
}

void ntfs_collect_mft_items(NTFS* ntfs, MftItem* first_item, int32_t first_item_index, MftItem** out_mft_items) {
	int32_t last_index = first_item_index;
	if (first_item->item_total > 1) {
		if (last_index == -1) {
			// nalezení indexu první položky
			ntfs_find_item_by_uid(ntfs, first_item->uid, 0, &last_index);
		}
		out_mft_items[0] = first_item;

		for (int i = 1; i < first_item->item_total; ++i) {
			MftItem* next_item = ntfs_find_item_by_uid(
					ntfs, first_item->uid, last_index + 1, &last_index);

			if (next_item == NULL) {
				ntfs_debug("Cannot find related MFT items\n");
				break;
			}

			out_mft_items[i] = next_item;
		}
	} else {
		out_mft_items[0] = first_item;
	}
}

void ntfs_collect_clusters_from_mft_items(
		NTFS* ntfs, MftItem* first_item, int32_t first_item_index, int32_t* out_cluster_ids) {

	MftItem **mft_items = (MftItem**) utils_malloc((first_item->item_total) * sizeof(MftItem*));
	ntfs_collect_mft_items(ntfs, first_item, first_item_index, mft_items);

	int32_t cluster_index = 0;
	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* item = mft_items[i];

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &(item->fragments[j]);

			for (int k = 0; k < fragment->count; ++k) {
				out_cluster_ids[cluster_index++] = fragment->start_index + k;
			}
		}
	}
	free(mft_items);
}

void ntfs_collect_clusters_from_fragments(
		MftFragment* fragments, int fragment_count, int32_t* out_cluster_ids) {

	int cluster_index = 0;
	for (int i = 0; i < fragment_count; ++i) {
		MftFragment* fragment = &fragments[i];
		for (int j = 0; j < fragment->count; ++j) {
			out_cluster_ids[cluster_index++] = fragment->start_index + j;
		}
	}
}

int32_t ntfs_count_free_space(NTFS* ntfs) {
	assert(ntfs->initialized);
	return ntfs_bitmap_count_free_clusters(ntfs->bitmap) * ntfs->boot_record->cluster_size;
}

void ntfs_lookup_table_fill(NTFS* ntfs, MftItem** out_table) {
	// nastavení všeho na NULL
	for (int i = 0; i < ntfs->bitmap->cluster_count; ++i) {
		out_table[i] = NULL;
	}

	// vyplnění pole
	for (int i = 0; i < ntfs->mft->length; ++i) {
		MftItem* item = &(ntfs->mft->data[i]);
		if (ntfs_mft_is_empty(item)) continue;

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &(item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) continue;

			for (int k = 0; k < fragment->count; ++k) {
				out_table[fragment->start_index + k] = item;
			}
		}
	}
}

void ntfs_lookup_table_update(NTFS* ntfs, MftItem** out_table,
							 MftItem* first_item, int32_t first_item_index, bool used) {

	MftItem **mft_items = (MftItem**) utils_malloc((first_item->item_total) * sizeof(MftItem*));
	ntfs_collect_mft_items(ntfs, first_item, first_item_index, mft_items);

	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* item = mft_items[i];

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &(item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) continue;

			for (int k = 0; k < fragment->count; ++k) {
				out_table[fragment->start_index + k] = (used) ? item : NULL;
			}
		}
	}
	free(mft_items);
}