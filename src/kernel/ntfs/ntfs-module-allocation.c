
/**
 * Implementuje funkce, které umožňují alokovat a uvolňovat různé zdroje.
 *
 * @author: Patrik Harag
 * @version: 2018-11-14
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "cpp-bridge.h"
#include "ntfs-module-allocation.h"
#include "ntfs-module-utils.h"
#include "ntfs-module-find.h"
#include "ntfs-io.h"
#include "ntfs-debug.h"


bool DISABLE_FIRST_FIT = false;


int32_t ntfs_resize_file(NTFS *ntfs, MftItem *first_item, int32_t target_size_in_bytes) {
	if (target_size_in_bytes > first_item->item_size) {
		// extend
		ntfs_extend_file(ntfs, first_item, target_size_in_bytes - first_item->item_size);
	}
	else if (target_size_in_bytes < first_item->item_size) {
		// trim
		ntfs_trim_file(ntfs, first_item, target_size_in_bytes);
	}
	return first_item->item_size;
}

int32_t ntfs_extend_file(NTFS *ntfs, MftItem *first_item, int32_t bytes) {
	// zkusíme to bez alokace - poslední cluster nemusí být zaplněn
	int32_t extended = ntfs_extend_file_last_cluster(ntfs, first_item, bytes);
	if (extended < bytes) {
		// zkusíme prodloužit poslední fragment alokací následujících clusterů
		MftFragment *last_non_empty_fragment = ntfs_find_last_not_empty_mft_item(ntfs, first_item);
		if (last_non_empty_fragment != NULL) {
			extended += ntfs_extend_file_last_fragment(ntfs, first_item, bytes - extended, last_non_empty_fragment);
		}

		if (extended < bytes) {
			extended += ntfs_extend_file_using_new_fragments(ntfs, first_item, bytes - extended, last_non_empty_fragment);
		}
	}
	return extended;
}

int32_t ntfs_extend_file_last_cluster(NTFS *ntfs, MftItem *first_item, int32_t bytes) {
	if (first_item->item_size != 0 && first_item->item_size % ntfs->boot_record->cluster_size != 0) {
		int32_t free_size = ntfs->boot_record->cluster_size - (first_item->item_size % ntfs->boot_record->cluster_size);
		if (free_size >= bytes) {
			first_item->item_size += bytes;
			return bytes;
		}
	}
	return 0;
}

int32_t ntfs_extend_file_last_fragment(NTFS *ntfs, MftItem *first_item, int32_t bytes, MftFragment *last_non_empty_fragment) {
	int32_t following_cluster_index = last_non_empty_fragment->start_index + last_non_empty_fragment->count;
	int32_t clusters_needed = utils_div_ceil(bytes, ntfs->boot_record->cluster_size);
	int32_t clusters_available = ntfs_bitmap_fragment_size(ntfs->bitmap, following_cluster_index, clusters_needed);
	if (clusters_available > 0) {
		// update mft
		last_non_empty_fragment->count += clusters_available;
		first_item->item_size += clusters_available * ntfs->boot_record->cluster_size;
		// update bitmapy
		ntfs_bitmap_set_all(ntfs->bitmap, last_non_empty_fragment, 1, true);

		return clusters_available * ntfs->boot_record->cluster_size;
	}
	return 0;
}

int32_t ntfs_extend_file_using_new_fragments(NTFS *ntfs, MftItem *first_item, int32_t bytes, MftFragment *last_non_empty_fragment) {
	// nalezení volných fragmentů
	int32_t min_cluster_index = last_non_empty_fragment != NULL
		? last_non_empty_fragment->start_index + last_non_empty_fragment->count
		: 0;
	int32_t clusters_needed = utils_div_ceil(bytes, ntfs->boot_record->cluster_size);
	int32_t free_fragment_count = 0;
	MftFragment* free_fragments = ntfs_find_free_clusters(ntfs, clusters_needed, min_cluster_index, &free_fragment_count);

	int32_t fragment_index = 0;
	int32_t extended = 0;
	int32_t to_extend = bytes;
	
	// využití volných fragmentů v mft položkách
	int32_t last_mft_item_index = -1;
	for (int i = 0; i < first_item->item_total && fragment_index < free_fragment_count; ++i) {
		MftItem* next_item = ntfs_find_next_mft_item(ntfs, first_item, i, &last_mft_item_index);
		if (next_item == NULL) {
			break;  // fs je poškozen
		}

		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT && fragment_index < free_fragment_count; ++j) {
			MftFragment *fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				// tento fragment není použit - a nebudou oni ostatní, protože se alokují postupně
				fragment->count = free_fragments[fragment_index].count;
				fragment->start_index = free_fragments[fragment_index].start_index;

				int32_t used = utils_min(fragment->count * ntfs->boot_record->cluster_size, to_extend);
				extended += used;
				to_extend -= used;

				ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, true);
				fragment_index++;
			}
		}
	}

	// alokace dalších mft položek, je-li potřeba
	int32_t fragments_remaining = free_fragment_count - fragment_index;
	if (fragments_remaining > 0) {
		// nalezení volných položek v MFT
		// podle počtu fragmentů se určí počet mft items
		int32_t new_mft_item_count = utils_div_ceil(fragments_remaining, NTFS_MFT_FRAGMENTS_COUNT);
		MftItem **free_mft_items = (MftItem**)utils_malloc((new_mft_item_count) * sizeof(MftItem*));

		last_mft_item_index = (last_mft_item_index == -1) ? 0 : last_mft_item_index + 1;
		new_mft_item_count = ntfs_mft_find_free_mft_items(ntfs->mft, last_mft_item_index, new_mft_item_count, free_mft_items);
		for (int32_t i = 0; i < new_mft_item_count; i++) {

			MftItem* next_item = free_mft_items[i];
			ntfs_mft_init_mft_item(next_item);  // vyčištění - pro jistou
			next_item->uid = first_item->uid;
			next_item->item_order = first_item->item_total++;

			for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT && fragment_index < free_fragment_count; ++j) {
				MftFragment *fragment = &(next_item->fragments[j]);
				fragment->count = free_fragments[fragment_index].count;
				fragment->start_index = free_fragments[fragment_index].start_index;

				int32_t used = utils_min(fragment->count * ntfs->boot_record->cluster_size, to_extend);
				extended += used;
				to_extend -= used;

				ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, true);
				fragment_index++;
			}
		}
		free(free_mft_items);
	}

	first_item->item_size += extended;
	free(free_fragments);
	return extended;
}

MftFragment* ntfs_find_free_clusters(NTFS *ntfs, int32_t n, int32_t preferred_min_cluster_index, int32_t *fragment_count) {
	if (n == 0) {
		*fragment_count = 0;
		return NULL;
	}

	if (!DISABLE_FIRST_FIT) {
		// prvně se pokusíme najít clustery za sebou
		MftFragment *fragments = ntfs_bitmap_find_free_clusters_first_fit(ntfs->bitmap, n, preferred_min_cluster_index);
		if (fragments != NULL) {
			*fragment_count = 1;
			return fragments;
		}
	}

	// když nenajdeme clustery za sebou, vezmeme je jak přijdou
	return ntfs_bitmap_find_free_clusters_take_first(ntfs->bitmap, n, preferred_min_cluster_index, fragment_count);
}

bool ntfs_trim_file(NTFS *ntfs, MftItem *first_item, int32_t target_size_in_bytes) {
	if (first_item->item_size == target_size_in_bytes) {
		return true;
	}

	bool drop = (target_size_in_bytes == 0);

	int32_t file_position = 0;
	int32_t mft_items = 0;

	int32_t last_mft_item_index = -1;
	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* next_item = ntfs_find_next_mft_item(ntfs, first_item, i, &last_mft_item_index);
		if (next_item == NULL) {
			return false;  // fs je poškozen
		}

		if (drop) {
			// zahození celého mft item
			if (i == 0) {
				// první se nemaže úplně
				ntfs_clean_mft_item(ntfs, next_item, false, true);
				mft_items++;
			} else {
				ntfs_clean_mft_item(ntfs, next_item, true, true);
			}

		} else {
			mft_items++;

			for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
				MftFragment* fragment = &(next_item->fragments[j]);
				if (ntfs_mft_fragment_is_empty(fragment)) {
					break;  // tento fragment není použit - a nebudou oni ostatní, protože se alokují postupně
				}

				if (drop) {
					// vyčištění fragmentu
					ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, false);
					fragment->start_index = 0;
					fragment->count = 0;

				} else {
					int32_t fragment_size_in_bytes = fragment->count * ntfs->boot_record->cluster_size;
					if (file_position + fragment_size_in_bytes <= target_size_in_bytes) {
						// přeskočení celého fragmentu
						file_position += fragment_size_in_bytes;
						if (file_position == target_size_in_bytes) {
							drop = true;
						}
					} else {
						// oříznutí kusu fragmentu
						int32_t index = target_size_in_bytes - (file_position + fragment_size_in_bytes);
						ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, false);

						fragment->count = utils_div_ceil(index, ntfs->boot_record->cluster_size);
						ntfs_bitmap_set_all(ntfs->bitmap, fragment, 1, true);
						drop = true;
					}
				}
			}
		}
	}

	first_item->item_size = target_size_in_bytes;
	first_item->item_total = mft_items;
	return true;
}

void ntfs_release_clusters(NTFS *ntfs, MftFragment* fragments, int count) {
	// update bitmapy
	ntfs_bitmap_set_all(ntfs->bitmap, fragments, count, false);
}

bool ntfs_allocate_mft_items(NTFS *ntfs, int32_t n, MftItem** out_items, int32_t uid) {
	if (ntfs_mft_find_free_mft_items(ntfs->mft, 0, n, out_items) == n) {
		for (int i = 0; i < n; ++i) {
			out_items[i]->uid = uid;
		}
		return true;
	}
	return (n == 0);
}

void ntfs_release_mft_items(NTFS *ntfs, MftItem** items, int32_t n) {
	for (int i = 0; i < n; ++i) {
		ntfs_mft_init_mft_item(items[i]);
	}
}
