
/**
 * Implementace všech příkazů.
 *
 * @author: Patrik Harag
 * @version:  2018-11-14
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "ntfs-console.h"
#include "ntfs-io.h"
#include "ntfs-module-find.h"
#include "ntfs-module-validation.h"
#include "ntfs-module-allocation.h"
#include "ntfs-module-utils.h"
#include "ntfs-debug.h"
#include "cpp-bridge.h"


Result command_init(ConsoleState* console_state, char** argv, int argc) {
	NTFS* ntfs = console_state->ntfs;

	int32_t disk_size_bytes = NTFS_DEFAULT_DISK_SIZE;
	int32_t cluster_size = NTFS_DEFAULT_CLUSTER_SIZE;
	int32_t mft_item_count;

	if (argc == 1 || argc == 2) {
		// parametrem je celková velikost disku

		if (argc >= 2) {
			disk_size_bytes = atoi(argv[1]);
		}

		mft_item_count = ntfs_mft_count_size(disk_size_bytes);

	} else if (argc == 3) {
		// parametry jsou počet clusterů a počet mft položek

		disk_size_bytes = atoi(argv[1]) * cluster_size;  // samotná data
		mft_item_count = atoi(argv[2]);

	} else {
		// parametry jsou počet clusterů, velikost clusteru, počet mft položek

		cluster_size = atoi(argv[2]);
		disk_size_bytes = atoi(argv[1]) * cluster_size;  // samotná data
		mft_item_count = atoi(argv[3]);
	}

	// validace

	if (disk_size_bytes % cluster_size != 0) {
		ntfs_debug("Size cannot be divided by cluster size\n");
		return RESULT_WARNING;
	}

	if (cluster_size % 4 != 0) {
		// velikost int32_t
		ntfs_debug("Cluster size cannot be divided by 4\n");
		return RESULT_WARNING;
	}

	// operaci je možné provést

	// vytvoříme nový
	MftItem* root = ntfs_init(ntfs, disk_size_bytes, cluster_size, mft_item_count);
	console_state->dir = root;

	if (root != NULL) {
		return RESULT_OK;
	}
	return RESULT_ERROR;
}

Result command_df(NTFS* ntfs) {
	int32_t free = ntfs_count_free_space(ntfs);
	int32_t total = ntfs->boot_record->disk_size;

	ntfs_debug("Total space [B]: %d\n", total);
	ntfs_debug("Free space [B]: %d\n", free);
	ntfs_debug("Used space [B]: %d (%.2f %%)\n",
		   total - free, (total - free) / (float) total * 100);

	return RESULT_OK;
}

Result command_show_parameters(NTFS* ntfs) {
	ntfs_debug("Signature: %s\n", ntfs->boot_record->signature);
	ntfs_debug("Volume descriptor: %s\n", ntfs->boot_record->volume_descriptor);
	ntfs_debug("Disk size [B]: %d\n", ntfs->boot_record->disk_size);
	ntfs_debug("Cluster size [B]: %d\n", ntfs->boot_record->cluster_size);
	ntfs_debug("Cluster count: %d\n", ntfs->boot_record->cluster_count);
	ntfs_debug("Bitmap size [B] %d\n", ntfs->boot_record->bitmap_size);
	ntfs_debug("MFT size [B] %ld\n", ntfs->boot_record->mft_item_count * (int32_t) sizeof(MftItem));
	ntfs_debug("MFT items count %d\n", ntfs->boot_record->mft_item_count);
	return RESULT_OK;
}

Result command_show_bitmap(NTFS* ntfs) {
	const int LINE_BREAK_AFTER = 100;
	int without_line_break = 0;

	for (int i = 0; i < ntfs->bitmap->cluster_count; ++i) {
		ntfs_debug("%c", '0' + ntfs_bitmap_is_cluster_used(ntfs->bitmap, i));

		without_line_break++;

		if (without_line_break == LINE_BREAK_AFTER) {
			ntfs_debug("\n");
			without_line_break = 0;
		}
	}
	ntfs_debug("\n");
	return RESULT_OK;
}

Result command_show_mft(NTFS* ntfs) {
	for (int i = 0; i < ntfs->mft->length; ++i) {
		MftItem* item = &(ntfs->mft->data[i]);

		if (item->uid == NTFS_MFT_UID_ITEM_FREE) {
			ntfs_debug("%-4d<empty>\n", i);
		} else {
			ntfs_debug("%-4d%-12d (%d/%d) %-15s %s %8d B  ",
				   i, item->uid, item->item_order + 1, item->item_total, item->item_name,
				   item->attributes & NTFS_ATTRIBUTE_DIRECTORY ? "dir " : "file", item->item_size);

			for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
				MftFragment* fragment = &item->fragments[j];
				if (!ntfs_mft_fragment_is_empty(fragment))
					ntfs_debug(" [s=%d, c=%d],", fragment->start_index, fragment->count);
			}
			ntfs_debug("\n");
		}
	}
	ntfs_debug("Free MFT items: %d / %d\n",
		   ntfs_mft_count_free_mft_items(ntfs->mft), ntfs->boot_record->mft_item_count);
	return RESULT_OK;
}

Result command_pwd(ConsoleState* console_state) {
	NTFS* ntfs = console_state->ntfs;

	char path[REPL_PATH_MAX_LENGTH + 1];  // + \0
	memset(path, 0, REPL_PATH_MAX_LENGTH + 1);

	MftItem* dir = console_state->dir;
	uint32_t index = REPL_PATH_MAX_LENGTH - 1;
	do {
		path[index--] = '/';

		if (dir->parent_uid == NTFS_MFT_UID_ITEM_FREE) {
			// root
			break;
		}

		uint32_t dir_name_len = (uint32_t) strlen(dir->item_name);
		if (dir_name_len + 2 > index) {  // + místo pro / a případně ~
			path[index--] = '~';
			break;
		} else {
			index -= dir_name_len;
			utils_strncpy(path + index + 1, dir->item_name, dir_name_len);
		}

		MftItem* next_dir = ntfs_find_item_by_uid(ntfs, dir->parent_uid, 0, NULL);
		if (next_dir == NULL) {
			ntfs_debug("Broken reference (parent_uid) uid: %d\n", dir->uid);
			return RESULT_WARNING;
		}
		dir = next_dir;

	} while (dir != NULL);

	ntfs_debug("%s\n", path + index + 1);
	return RESULT_OK;
}

Result command_cd(ConsoleState* console_state, char* path) {
	MftItem* item = ntfs_find(console_state->ntfs, path, console_state->dir);
	if (item == NULL || !(item->attributes & NTFS_ATTRIBUTE_DIRECTORY)) {
		ntfs_debug("PATH NOT FOUND\n");
		return RESULT_WARNING;

	} else {
		console_state->dir = item;

		ntfs_debug("OK\n");
		return RESULT_OK;
	}
}

Result command_ls(ConsoleState* console_state, char* path) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* dir;
	if (strlen(path) == 0 || strcmp(path, ".") == 0) {
		// current dir
		dir = console_state->dir;
	} else {
		dir = ntfs_find(ntfs, path, console_state->dir);

		if (dir == NULL || !(dir->attributes & NTFS_ATTRIBUTE_DIRECTORY)) {
			ntfs_debug("PATH NOT FOUND\n");
			return RESULT_WARNING;
		}
	}

	if (dir->item_size == 0) {
		// prázdný adresář
		return RESULT_OK;
	}

	// načtení seznamu souborů
	char *data = io_file_read_all(ntfs, dir);
	if (data == NULL) {
		return RESULT_ERROR;
	}

	int entries = dir->item_size / 4;
	for (int i = 0; i < entries; ++i) {
		int32_t uid = ((int32_t*) data)[i];

		int32_t index;
		MftItem* next_item = ntfs_find_item_by_uid(ntfs, uid, 0, &index);

		if (next_item == NULL) {
			ntfs_debug("?%d\n", uid);
		} else {
			ntfs_debug("%s%s\n", next_item->attributes & NTFS_ATTRIBUTE_DIRECTORY ? "+" : "-", next_item->item_name);
		}
	}
	free(data);
	return RESULT_OK;
}

Result command_mkdir(ConsoleState* console_state, char* path) {
	NTFS* ntfs = console_state->ntfs;

	char *name = (char*) utils_malloc((strlen(path) + 1) * sizeof(char));
	MftItem* dir = ntfs_find_parent(ntfs, path, console_state->dir, name);

	if (dir == NULL) {
		ntfs_debug("PATH NOT FOUND\n");
		free(name);
		return RESULT_WARNING;
	}

	if (!ntfs_check_name(name)) {
		ntfs_debug("FILENAME NOT VALID\n");
		free(name);
		return RESULT_WARNING;
	}

	if (ntfs_find_in_dir(ntfs, name, dir) != NULL) {
		ntfs_debug("EXIST\n");
		free(name);
		return RESULT_WARNING;
	}

	MftItem* new_dir = ntfs_create_empty_file(ntfs, name, dir, NTFS_ATTRIBUTE_DIRECTORY);
	if (new_dir == NULL) {
		// nedostatek místa atd...
		free(name);
		return RESULT_WARNING;
	}

	free(name);
	ntfs_debug("OK\n");
	return RESULT_OK;
}

Result command_rmdir(ConsoleState* console_state, char* name) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, name, console_state->dir);
	if (first_item == NULL) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	if (!(first_item->attributes & NTFS_ATTRIBUTE_DIRECTORY)) {
		ntfs_debug("NOT DIRECTORY\n");
		return RESULT_WARNING;
	}

	if (first_item->item_size > 0) {
		ntfs_debug("NOT EMPTY\n");
		return RESULT_WARNING;
	}

	if (first_item->parent_uid == NTFS_MFT_UID_ITEM_FREE) {
		ntfs_debug("ROOT DIRECTORY\n");
		return RESULT_WARNING;
	}

	if (first_item->uid == console_state->dir->uid) {
		ntfs_debug("WORKING DIRECTORY\n");
		return RESULT_WARNING;
	}

	MftItem* dir = ntfs_find_item_by_uid(ntfs, first_item->parent_uid, 0, NULL);
	if (dir == NULL) {
		ntfs_debug("Cannot find parent dir\n");
		return RESULT_ERROR;
	}

	ntfs_remove(ntfs, first_item, dir);

	ntfs_debug("OK\n");
	return RESULT_OK;
}

Result command_create(ConsoleState* console_state, char* dest_path) {
	NTFS* ntfs = console_state->ntfs;
	
	char *name = (char*)utils_malloc((strlen(dest_path) + 1) * sizeof(char));
	MftItem* dir = ntfs_find_parent(ntfs, dest_path, console_state->dir, name);

	if (!ntfs_check_new_item(ntfs, dir, name)) {
		free(name);
		return RESULT_WARNING;
	}

	MftItem *item = ntfs_create_empty_file(ntfs, name, dir, 0);
	if (item) {
		ntfs_debug("OK\n");
	}
	free(name);
	return item ? RESULT_OK : RESULT_WARNING;
}

Result command_info(ConsoleState* console_state, char* path) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, path, console_state->dir);
	if (first_item == NULL) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}
	MftItem **mft_items = (MftItem**) utils_malloc((first_item->item_total) * sizeof(MftItem*));
	ntfs_collect_mft_items(ntfs, first_item, -1, mft_items);

	ntfs_debug("Name: %s\n", first_item->item_name);
	ntfs_debug("UID: %d\n", first_item->uid);
	ntfs_debug("Size: %d B\n", first_item->item_size);

	ntfs_debug("Fragments: ");
	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* next_item = mft_items[i];
		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				break;  // not used
			}

			ntfs_debug("[start=%d, count=%d], ", fragment->start_index, fragment->count);
		}
	}
	ntfs_debug("\n");

	ntfs_debug("Clusters: ");
	for (int i = 0; i < first_item->item_total; ++i) {
		MftItem* next_item = mft_items[i];
		for (int j = 0; j < NTFS_MFT_FRAGMENTS_COUNT; ++j) {
			MftFragment* fragment = &(next_item->fragments[j]);
			if (ntfs_mft_fragment_is_empty(fragment)) {
				break;  // not used
			}

			for (int k = 0; k < fragment->count; ++k) {
				ntfs_debug("%d, ", fragment->start_index + k);
			}
		}
	}
	ntfs_debug("\n");
	free(mft_items);
	return RESULT_OK;
}

Result command_incp(ConsoleState* console_state, char* src_path, char* dest_path) {
	NTFS* ntfs = console_state->ntfs;
	FILE *src_file;
	fopen_s(&src_file, src_path, "rb");

	if (src_file == NULL) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;

	} else {
		char *name = (char*) calloc(strlen(dest_path) + 1, sizeof(char));
		MftItem* dir = ntfs_find_parent(ntfs, dest_path, console_state->dir, name);

		if (!ntfs_check_new_item(ntfs, dir, name)) {
			free(name);
			return RESULT_WARNING;
		}

		fseek(src_file, 0, SEEK_END);
		int32_t size = ftell(src_file);
		rewind(src_file);

		char *buffer = (char *)utils_malloc((size + 1) * sizeof(char));
		fread(buffer, size, 1, src_file);
		fclose(src_file);

		MftItem* item = ntfs_create_file(ntfs, name, buffer, size, dir);

		free(name);
		free(buffer);

		if (item) {
			ntfs_debug("OK\n");
		}

		return item ? RESULT_OK : RESULT_WARNING;
	}
}

Result command_rm(ConsoleState* console_state, char* path) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, path, console_state->dir);

	if (first_item == NULL) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	if (first_item->attributes & NTFS_ATTRIBUTE_DIRECTORY) {
		ntfs_debug("NOT FILE\n");
		return RESULT_WARNING;
	}

	MftItem* dir = ntfs_find_item_by_uid(ntfs, first_item->parent_uid, 0, NULL);
	if (dir == NULL) {
		ntfs_debug("Cannot find parent dir\n");
		return RESULT_ERROR;
	}

	ntfs_remove(ntfs, first_item, dir);
	ntfs_debug("OK\n");
	return RESULT_OK;
}

Result command_read(ConsoleState* console_state, char* path, int32_t offset, int32_t size) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, path, console_state->dir);
	if (first_item == NULL || first_item->attributes & NTFS_ATTRIBUTE_DIRECTORY) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	if (size == -1) {
		size = first_item->item_size;
	}

	char *data = (char*)utils_malloc((size + 1) * sizeof(char));
	if (io_file_read(ntfs, first_item, offset, data, size) != size) {
		ntfs_debug("IO FAILURE\n");
		free(data);
		return RESULT_ERROR;
	}

	// nahrazení 0 za # - chceme to vidět celé
	for (size_t i = 0; i < size; i++) {
		if (data[i] == '\0') data[i] = '#';
	}
	data[size] = '\0';
	ntfs_debug("%s\n", data);
	free(data);

	return RESULT_OK;
}

Result command_write(ConsoleState* console_state, char* path, int32_t offset, char *data, int32_t data_size) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, path, console_state->dir);
	if (first_item == NULL || first_item->attributes & NTFS_ATTRIBUTE_DIRECTORY) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	if (offset == -1) {
		offset = first_item->item_size;
	}

	if (io_file_write(ntfs, first_item, offset, data, data_size) != data_size) {
		ntfs_debug("IO FAILURE\n");
		return RESULT_ERROR;
	}
	ntfs_debug("OK\n");

	return RESULT_OK;
}

Result command_resize(ConsoleState* console_state, char* path, int32_t data_size) {
	NTFS* ntfs = console_state->ntfs;

	MftItem* first_item = ntfs_find(ntfs, path, console_state->dir);
	if (first_item == NULL || first_item->attributes & NTFS_ATTRIBUTE_DIRECTORY) {
		ntfs_debug("FILE NOT FOUND\n");
		return RESULT_WARNING;
	}

	if (ntfs_resize_file(ntfs, first_item, data_size) != data_size) {
		ntfs_debug("IO FAILURE\n");
		return RESULT_ERROR;
	}
	ntfs_debug("OK\n");

	return RESULT_OK;
}
