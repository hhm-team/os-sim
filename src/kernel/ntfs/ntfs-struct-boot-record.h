#pragma once

/**
 * Definuje nízkoúrovňové funkce pro práci s boot record.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#include <stdint.h>

#define BOOT_RECORD_DESCRIPTOR "ntfs-v1.0"

// struktury

/** Struktura boot record tak, jak se bude ukládat do souboru */
typedef struct _BootRecord {
	char signature[16];			  // login autora FS
	char volume_descriptor[16];	  // popis vygenerovaného FS
	int32_t disk_size;			   // maximální velikost obsahu
	int32_t cluster_size;			// velikost clusteru
	int32_t cluster_count;		   // pocet clusteru
	int32_t mft_item_count;		  // počet položek v mft
	int32_t bitmap_size;			 // velikost bitmapy v bytech
} BootRecord;

// funkce

/** Inicializuje strukturu */
void ntfs_boot_record_init(BootRecord *boot_record, int32_t disk_size,
						  int32_t cluster_size, int32_t mft_size);

/** Uvolní strukturu */
void ntfs_boot_record_free(BootRecord *boot_record);
