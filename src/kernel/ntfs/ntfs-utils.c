
/**
 * Implementuje utility funkce.
 *
 * @author: Patrik Harag
 * @version: 2018-10-26
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ntfs-utils.h"

int32_t utils_div_ceil(int32_t a, int32_t b) {
	int32_t result = a / b;
	if (a % b != 0) {
		result++;
	}
	return result;
}

int32_t utils_parse_int(char *str) {
	int32_t result = 0;
	int32_t puiss = 1;
	while (('-' == (*str)) || ((*str) == '+')) {
		if (*str == '-')
			puiss = puiss * -1;
		str++;
	}
	while ((*str >= '0') && (*str <= '9')) {
		result = (result * 10) + ((*str) - '0');
		str++;
	}
	return (result * puiss);
}

char** utils_split_by_space(const char* str, int* out_count) {
	char** out = NULL;
	size_t len = strlen(str);
	
	int i = 0;
	int n = 0;

	while (i < len) {
		if (str[i] == 32 /* mezera */) {
			i++;
		}
		else {
			int start = i;
			for (; i < len; i++) {
				if (str[i] == 32 /* mezera */) {
					break;
				}
			}
			int size = i - start;
			if (size > 0) {
				char* next = utils_calloc(size + 1, sizeof(char*));
				utils_strncpy(next, str + start, size);

				out = utils_realloc(out, sizeof(char*) * ++n);
				out[n - 1] = next;
			}
		}
	}
	
	*out_count = n;
	return out;
}

bool utils_is_bit_set(unsigned char value, int pos) {
	assert(pos >= 0 && pos < 8);
	return (value & (1 << pos)) != 0;
}

unsigned char utils_set_bit(unsigned char value, int pos) {
	assert(pos >= 0 && pos < 8);
	return (unsigned char) ((1 << pos) | value);
}

unsigned char utils_unset_bit(unsigned char value, int pos) {
	assert(pos >= 0 && pos < 8);
	return (unsigned char) (value & ~(1 << pos));
}

errno_t utils_strncpy(char *dest, const char *src, size_t n) {
	return strncpy_s(dest, n+1 /* \0 */, src, n);
}

void* utils_malloc(size_t size) {
	void *ptr = malloc(size);
	assert(ptr != NULL);
	return ptr;
}

void* utils_calloc(size_t count, size_t type_size) {
	void *ptr = calloc(count, type_size);
	assert(ptr != NULL);
	return ptr;
}

void* utils_realloc(void* old_ptr, size_t size) {
	void *ptr = realloc(old_ptr, size);
	if (!ptr) {
		free(old_ptr);
		assert(false);
	}
	return ptr;
}
