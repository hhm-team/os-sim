#pragma once

/**
 * Debug záležitosti.
 *
 * @author: Patrik Harag
 * @version: 2018-10-20
 */

#define DEBUG_PRINT_BUFFER_SIZE 1024 * 2

void ntfs_debug(char const* fmt, ...);
