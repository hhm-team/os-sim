#include "ntfs-api.h"
#include <memory>
#include <string>
#include <algorithm>

extern "C" {
#include "ntfs-io.h"
#include "ntfs-module-find.h"
#include "ntfs-module-validation.h"
#include "ntfs-module-allocation.h"
#include "ntfs-console.h"
#include "ntfs-console-commands.h"
}

File_System_NTFS::File_System_NTFS(uint8_t disk_drive_number, kiv_hal::TDrive_Parameters drive_parameters) {
	this->disk_drive_number = disk_drive_number;
	this->drive_parameters = drive_parameters;

	this->ntfs = ntfs_create(disk_drive_number, drive_parameters.bytes_per_sector);
	// na�ten� boot recordu ze za��tku disku a podle n�j na�ten� ostatn�ch struktur
	// nebo jeho vytvo�en� a naform�tov�n� disku o uveden�ch parametrech
	this->root_dir = ntfs_load_or_init_disk_of_total_size(ntfs, Get_Total_Size(), drive_parameters.bytes_per_sector);
	this->debug_working_dir = root_dir;

	Clear_Error_Code();
}

void File_System_NTFS::Run_Debug_Command(const char *command) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	ConsoleState console_state;
	console_state.ntfs = this->ntfs;
	console_state.dir = this->debug_working_dir;
	console_eval_command(&console_state, command);
	this->debug_working_dir = console_state.dir;

	Clear_Error_Code();
}

MftItem* File_System_NTFS::Get_Root_Dir() {
	return root_dir;
}

MftItem* File_System_NTFS::Find_File(const char *path, MftItem *working_dir) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	MftItem* result = ntfs_find(ntfs, path, working_dir);
	Clear_Error_Code();
	return result;
}

MftItem* File_System_NTFS::Find_File(int32_t uid) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	int32_t out_index = 0;
	MftItem* result = ntfs_find_item_by_uid(ntfs, uid, 0, &out_index);
	Clear_Error_Code();
	return result;
}

MftItem* File_System_NTFS::Create_File(const char *name, MftItem *dir, uint8_t attributes, kiv_os::NOS_Error *out_error_code) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	MftItem *item = ntfs_create_empty_file(ntfs, name, dir, attributes);

	*out_error_code = Get_Error_Code();
	Clear_Error_Code();

	return item;
}

void File_System_NTFS::Remove_File(MftItem *file, kiv_os::NOS_Error *out_error_code) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	if ((file->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) && file->item_size > 0) {
		*out_error_code = kiv_os::NOS_Error::Directory_Not_Empty;
		return;
	}

	if (file->parent_uid == 0) {
		// ko�enov� adres�� nelze odstranit...
		*out_error_code = kiv_os::NOS_Error::Permission_Denied;
		return;
	}

	int32_t out_index = 0;
	MftItem* dir = ntfs_find_item_by_uid(ntfs, file->parent_uid, 0, &out_index);
	if (dir != NULL) {
		ntfs_remove(ntfs, file, dir);
		*out_error_code = Get_Error_Code();
	}
	else {
		// asi n�jak� nekonzistence...
		kiv_os::NOS_Error err = Get_Error_Code();
		*out_error_code = (err == kiv_os::NOS_Error::Success) ? kiv_os::NOS_Error::Unknown_Error : err;
	}
	Clear_Error_Code();
}

int32_t File_System_NTFS::Resize_File(MftItem *file, int32_t new_size_in_bytes, kiv_os::NOS_Error *out_error_code) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	if (file->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
		*out_error_code = kiv_os::NOS_Error::Permission_Denied;
		return file->item_size;
	}

	int32_t actual_size = ntfs_resize_file(ntfs, file, new_size_in_bytes);

	*out_error_code = Get_Error_Code();
	Clear_Error_Code();

	if (actual_size != new_size_in_bytes && *out_error_code == kiv_os::NOS_Error::Success) {
		*out_error_code = kiv_os::NOS_Error::Not_Enough_Disk_Space;
	}

	return actual_size;
}

int32_t File_System_NTFS::Write(MftItem *file, int32_t offset_in_bytes, const void *data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	if (file->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
		// do adres��e se nep�e p��mo, od toho je Create_File
		*out_error_code = kiv_os::NOS_Error::Permission_Denied;
		return 0;
	}

	int32_t bytes_written = io_file_write(ntfs, file, offset_in_bytes, data, data_size_in_bytes);

	*out_error_code = Get_Error_Code();
	Clear_Error_Code();

	if (bytes_written != data_size_in_bytes && *out_error_code == kiv_os::NOS_Error::Success) {
		*out_error_code = kiv_os::NOS_Error::Not_Enough_Disk_Space;
	}

	return bytes_written;
}

int32_t File_System_NTFS::Read(MftItem *file, int32_t offset_in_bytes, void *data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) {
	std::lock_guard<std::recursive_mutex> lock(mutex);

	int32_t bytes_read = io_file_read(ntfs, file, offset_in_bytes, data, data_size_in_bytes);

	*out_error_code = Get_Error_Code();
	Clear_Error_Code();

	/*
	if (bytes_read != data_size_in_bytes && *out_error_code == kiv_os::NOS_Error::Success) {
		*out_error_code = kiv_os::NOS_Error::Out_Of_Memory;
	}
	*/

	return bytes_read;
}

uint32_t File_System_NTFS::Get_Total_Size() {
	return (uint32_t)drive_parameters.absolute_number_of_sectors * drive_parameters.bytes_per_sector;
}

void File_System_NTFS::Clear_Error_Code() {
	ntfs->disk_last_error = static_cast<uint16_t>(kiv_os::NOS_Error::Success);
}

kiv_os::NOS_Error File_System_NTFS::Get_Error_Code() {
	return static_cast<kiv_os::NOS_Error>(ntfs->disk_last_error);
}

void File_System_NTFS::Close() {
	io_update_mft(ntfs);
	io_update_bitmap(ntfs);
	ntfs_free(ntfs);
}
