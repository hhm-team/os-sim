
/**
 * Implementuje různé validační funkce.
 *
 * @author: Patrik Harag
 * @version: 2017-12-20
 */

#include <stdio.h>
#include "ntfs-module-validation.h"
#include "ntfs-module-find.h"
#include "ntfs-debug.h"


bool ntfs_check_name(char* name) {
	size_t len = strlen(name);

	if (len == 0)
		return false;

	for (int i = 0; i < len; ++i) {
		char c = name[i];
		if (c == '/') return false;
	}
	return true;
}

bool ntfs_check_new_item(NTFS* ntfs, MftItem* new_item_dir, char* new_item_name) {
	if (new_item_dir == NULL) {
		ntfs_debug("PATH NOT FOUND\n");
		return false;
	}

	if (!ntfs_check_name(new_item_name)) {
		ntfs_debug("FILENAME NOT VALID\n");
		return false;
	}

	if (ntfs_find_in_dir(ntfs, new_item_name, new_item_dir) != NULL) {
		ntfs_debug("EXIST\n");
		return false;
	}
	return true;
}