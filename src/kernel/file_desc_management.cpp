#include "file_desc_management.h"

File_Desc_Management::File_Desc_Management(std::shared_ptr<Handle_Generator> ptr, std::shared_ptr<File_System> fs_ptr) {
	generator = ptr;
	disk_management = fs_ptr;
	table = {};

	// V tabulce jsou po celou dobu cteni a zapis do konzole.
	table.insert({ 0, new StdIO(0, kiv_os::NFile_Attributes::Read_Only, "STDIN", 0) });
	table.insert({ 1, new StdIO(1, static_cast<kiv_os::NFile_Attributes>(0), "STDOUT", 0) });

	// Zjisteni prazdneho adresare.
	if (disk_management)
	{
		working_dir = disk_management->Get_Root_Dir();
	}
	else
	{
		// Nebyl nalezen zadny dostupny souborovy system.
		working_dir = nullptr;
	}
}

File_Desc_Management::~File_Desc_Management() {
	for (auto it = table.begin(); it != table.end(); ) {
		delete it->second;
		it = table.erase(it);
	}
}

Descriptor * File_Desc_Management::find_file_desc(kiv_os::THandle handle) {
	std::lock_guard<std::mutex> guard(mutex);
	auto it = table.find(handle);

	if (it == table.end()) {
		return nullptr;
	}

	else {
		return it->second;
	}
}

kiv_os::THandle File_Desc_Management::open_file(const char *file, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes, kiv_os::NOS_Error &result)
{
	std::lock_guard<std::mutex> guard(mutex);
	kiv_os::THandle handle = kiv_os::Invalid_Handle;

	if (!working_dir)
	{
		result = kiv_os::NOS_Error::IO_Error;
		return handle;
	}

	MftItem *item = disk_management->Find_File(file, working_dir);
	if (flags == static_cast<kiv_os::NOpen_File>(-1) || (item == NULL && flags != kiv_os::NOpen_File::fmOpen_Always))
	{
		// Jedna se o vytvoreni souboru (ten nesmi existovat).
		if (item == NULL)
		{
			item = disk_management->Create_File(file, working_dir, static_cast<std::uint8_t>(attributes), &result);
		}
		else
		{
			result = kiv_os::NOS_Error::IO_Error;
			return handle;
		}
	}
	else if (attributes == static_cast<kiv_os::NFile_Attributes>(-1))
	{
		// Otevreni souboru.
		if (item == NULL)
		{
			if (flags == kiv_os::NOpen_File::fmOpen_Always)
			{
				// Soubor musi existovat, aby byl otevren.
				result = kiv_os::NOS_Error::File_Not_Found;
				return handle;
			}
		}
		else if (flags != kiv_os::NOpen_File::fmOpen_Always)
		{
			// Prepsani souboru.
			MftItem *parent = disk_management->Find_File(item->parent_uid);
			disk_management->Remove_File(item, &result);
			if (result == kiv_os::NOS_Error::Success)
			{
				item = disk_management->Create_File(file, parent, static_cast<std::uint8_t>(attributes), &result);
			}
			else
			{
				return handle;
			}
		}
	}

	if (item != NULL)
	{
		// Operace probehly v poradku, zjistit handle a priradit ho k nove otevrenemu souboru.
		handle = generator->get_handle();
		FileIO *io = new FileIO(disk_management, handle, file, item);
		table.insert({ handle, io });
		result = kiv_os::NOS_Error::Success;
	}
	else
	{
		result = kiv_os::NOS_Error::IO_Error;
	}

	return handle;
}

bool File_Desc_Management::close_handle(kiv_os::THandle handle)
{
	bool result;

	if ((result = find_file_desc(handle) != nullptr) && handle > 1)
	{
		PipeIO *pipe = dynamic_cast<PipeIO *>(table.find(handle)->second);
		StdIO *is_std = dynamic_cast<StdIO *>(table.find(handle)->second);

		bool delete_item = true;
		if (pipe)
		{
			// Instance tridy PipeIO. Specialni ukoncovani, v tabulce otevrenych souboru dva handly na jeden objekt.
			delete_item = pipe->closing(handle);
		}

		if (is_std) {
			return true;
		}

		if (delete_item)
		{
			delete table.find(handle)->second;
		}

		table.erase(handle);
		generator->free_handle(handle);
	}

	return result;
}

void File_Desc_Management::remove_file(const char *dir_path, bool any, kiv_os::NOS_Error &result)
{
	std::lock_guard<std::mutex> guard(mutex);
	MftItem *item = disk_management->Find_File(dir_path, working_dir);
	if (item != NULL)
	{
		if (item != working_dir && (!(item->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::System_File)) || any))
		{
			disk_management->Remove_File(item, &result);
		}
		else
		{
			result = kiv_os::NOS_Error::Permission_Denied;
		}
	}
	else
	{
		result = kiv_os::NOS_Error::File_Not_Found;
	}
}

void File_Desc_Management::set_working_dir(const char *dir_path, kiv_os::NOS_Error &result)
{
	MftItem *item = disk_management->Find_File(dir_path, working_dir);
	if (item != NULL)
	{
		if (item->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))
		{
			working_dir = item;
			result = kiv_os::NOS_Error::Success;
		}
		else
		{
			result = kiv_os::NOS_Error::Invalid_Argument;
		}
	}
	else
	{
		result = kiv_os::NOS_Error::File_Not_Found;
	}
}

MftItem *File_Desc_Management::get_working_dir()
{
	return working_dir;
}

MftItem *File_Desc_Management::get_mft_item(int32_t id)
{
	return disk_management->Find_File(id);
}

void File_Desc_Management::create_pipe(kiv_os::THandle *array, kiv_os::NOS_Error &result)
{
	array[0] = generator->get_handle();
	array[1] = generator->get_handle();

	PipeIO *pipe = new PipeIO(array[0], array[1]);
	table.insert({ array[0], pipe });
	table.insert({ array[1], pipe });
	result = kiv_os::NOS_Error::Success;
}

/*------------------------------------End File_Desc_Management------------------------------------*/

size_t Read_Line_From_Console(char *buffer, const size_t buffer_size) {
	kiv_hal::TRegisters registers;
	memset(&registers, 0, sizeof(kiv_hal::TRegisters));

	size_t pos = 0;
	while (pos < buffer_size) {
		//read char
		registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NKeyboard::Read_Char);
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Keyboard, registers);

		if (!registers.flags.non_zero) break;	//nic jsme neprecetli, 
												//pokud je rax.l EOT, pak byl zrejme vstup korektne ukoncen
												//jinak zrejme doslo k chybe zarizeni

		char ch = registers.rax.l;

		//osetrime zname kody
		switch (static_cast<kiv_hal::NControl_Codes>(ch)) {
		case kiv_hal::NControl_Codes::BS: {
			//mazeme znak z bufferu
			if (pos > 0) pos--;

			registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NVGA_BIOS::Write_Control_Char);
			registers.rdx.l = ch;
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);
		}
										  break;

		case kiv_hal::NControl_Codes::LF:  break;	//jenom pohltime, ale necteme
		case kiv_hal::NControl_Codes::NUL:	return 0;		//chyba cteni?
		case kiv_hal::NControl_Codes::EOT:	return 0;
		case kiv_hal::NControl_Codes::CR:  return pos;	//docetli jsme az po Enter


		default: buffer[pos] = ch;
			pos++;
			registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NVGA_BIOS::Write_String);
			registers.rdx.r = reinterpret_cast<decltype(registers.rdx.r)>(&ch);
			registers.rcx.r = 1;
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);
			break;
		}
	}

	return pos;

}

/*------------------------------------Start StdIO------------------------------------*/

StdIO::StdIO(kiv_os::THandle handle, kiv_os::NFile_Attributes permission, const char* file, MftItem* mft_item) {
	this->handle = handle;
	this->permission = permission;
	this->file = file;
	this->mft_item = mft_item;
}

void StdIO::write(kiv_hal::TRegisters& regs) {
	//std::lock_guard<std::mutex> guard(mutex);
	if (static_cast<uint8_t>(permission) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only))
	{
		regs.flags.carry = 1;
	}
	else
	{
		kiv_hal::TRegisters registers;
		registers.rax.h = static_cast<decltype(registers.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
		registers.rdx.r = regs.rdi.r;
		registers.rcx = regs.rcx;

		//preklad parametru dokoncen, zavolame sluzbu
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);

		regs.flags.carry |= (registers.rax.r == 0 ? 1 : 0);	//jestli jsme nezapsali zadny znak, tak jiste doslo k nejake chybe
		regs.rax = registers.rcx;	//VGA BIOS nevraci pocet zapsanych znaku, tak predpokladame, ze zapsal vsechny
	}
}

void StdIO::read(kiv_hal::TRegisters& regs) {
	//std::lock_guard<std::mutex> guard(mutex);
	if (static_cast<uint8_t>(permission) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only)) {
		regs.rax.r = Read_Line_From_Console(reinterpret_cast<char*>(regs.rdi.r), regs.rcx.r);
	}

	else {
		regs.flags.carry = 1;
	}
}

/*------------------------------------End StdIO------------------------------------*/


/*------------------------------------Start FileIO------------------------------------*/

FileIO::FileIO(std::shared_ptr<File_System> fs, kiv_os::THandle handle, const char * file, MftItem* mft_item) {
	this->fs = fs;
	this->handle = handle;
	this->file = file;
	this->mft_item = mft_item;

	offset = 0;
	entries = nullptr;
	if (mft_item->attributes & static_cast<decltype(mft_item->attributes)>(kiv_os::NFile_Attributes::Directory))
	{
		// Adresar - vytvoreni pole s TDirEntry.
		create_TDirEntries();
	}
}

FileIO::~FileIO()
{
	if (entries != nullptr)
	{
		delete[] entries;
	}
}

void FileIO::create_TDirEntries()
{
	char *buffer = nullptr;
	size_t read;
	kiv_os::NOS_Error result;
	int counter = 0;

	// Seek muze byt drive nez read - potrebuji u slozky nacist obsah adresare a nejak nainicializovat buffer.
	// Iterativne navysovat buffer podle potreby tak, aby byl nacten cely adresar.
	while (true)
	{
		if (++counter > 1)
		{
			delete[] buffer;
		}

		const size_t buffer_size = sizeof(int32_t) * (counter * 1000);
		buffer = new char[buffer_size];
		memset(buffer, '\0', buffer_size);
		read = fs->Read(mft_item, offset, buffer, static_cast<int32_t>(buffer_size), &result);

		if (result != kiv_os::NOS_Error::Success || read < buffer_size)
		{
			// Bud doslo k chybe nebo byl nacten cely obsah adresare.
			break;
		}
	}

	if (result == kiv_os::NOS_Error::Success)
	{
		entries_count = read / 4;
		entries = new kiv_os::TDir_Entry[entries_count];

		int32_t *uids = reinterpret_cast<int32_t *>(buffer);
		for (int i = 0; i < entries_count; i++)
		{
			MftItem *file = fs->Find_File(uids[i]);
			memset(entries[i].file_name, '\0', NTFS_MFT_ITEM_NAME_LENGTH);
			strcpy_s(entries[i].file_name, NTFS_MFT_ITEM_NAME_LENGTH, file->item_name);
			entries[i].file_attributes = static_cast<decltype(entries[i].file_attributes)>(file->attributes);
		}
	}
	else
	{
		entries = nullptr;
	}

	if (buffer != nullptr)
	{
		delete[] buffer;
	}
}

void FileIO::write(kiv_hal::TRegisters &regs)
{
	std::lock_guard<std::mutex> guard(mutex);
	const char *buffer = reinterpret_cast<const char *>(regs.rdi.r);
	size_t buffer_size = static_cast<size_t>(regs.rcx.r);

	kiv_os::NOS_Error result;
	size_t written;
	if (mft_item->attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))
	{
		// Adresar je read-only.
		result = kiv_os::NOS_Error::Permission_Denied;
	}
	else
	{
		written = fs->Write(mft_item, offset, buffer, static_cast<int32_t>(buffer_size), &result);
	}

	if (result == kiv_os::NOS_Error::Success)
	{
		offset += static_cast<int32_t>(written);
		regs.rax.r = static_cast<decltype(regs.rax.r)>(written);
	}
	else
	{
		regs.flags.carry = true;
		regs.rax.r = static_cast<decltype(regs.rax.r)>(result);
	}
}

void FileIO::read(kiv_hal::TRegisters &regs)
{
	std::lock_guard<std::mutex> guard(mutex);
	char *buffer = reinterpret_cast<char *>(regs.rdi.r);
	size_t buffer_size = static_cast<size_t>(regs.rcx.r);

	kiv_os::NOS_Error result;
	int32_t read = 0;

	if (mft_item->attributes & static_cast<decltype(mft_item->attributes)>(kiv_os::NFile_Attributes::Directory))
	{
		if (entries == nullptr)
		{
			result = kiv_os::NOS_Error::IO_Error;
		}
		else
		{
			result = kiv_os::NOS_Error::Success;

			// buffer_size predstavuje pocet polozek, pro ktere je alokovane misto.
			read = std::min<int32_t>(static_cast<int32_t>(buffer_size), static_cast<int32_t>(entries_count) - offset); // Kolik polozek se ma precist (nezapomenout, ze muze byt nastaveny offset).

			if (read < 1)
			{
				// Pokud by byl offset mimo hranice.
				read = 0;
			}
			else
			{
				// Offset (seek), velikost bufferu a pocet polozek jsou metriky udavane v polozkach TDir_Entry (fyzicky se ale musi pracovat s byty).
				char *results = reinterpret_cast<char *>(entries);
				memset(buffer, '\0', buffer_size * sizeof(kiv_os::TDir_Entry));
				memcpy(buffer, results + offset * sizeof(kiv_os::TDir_Entry), read * sizeof(kiv_os::TDir_Entry));
			}
		}
	}
	else
	{
		read = fs->Read(mft_item, offset, (void *)buffer, static_cast<int32_t>(buffer_size), &result);
	}

	if (result == kiv_os::NOS_Error::Success)
	{
		offset += read;
		regs.rax.r = static_cast<decltype(regs.rax.r)>(read);
	}
	else
	{
		regs.flags.carry = true;
		regs.rax.r = static_cast<decltype(regs.rax.r)>(result);
	}
}

size_t FileIO::seek(kiv_os::THandle handle, size_t offset, kiv_os::NFile_Seek attribute, kiv_os::NOS_Error &result)
{
	std::lock_guard<std::mutex> guard(mutex);
	bool dir = mft_item->attributes & static_cast<decltype(mft_item->attributes)>(kiv_os::NFile_Attributes::Directory);
	if (dir && entries == nullptr)
	{
		result = kiv_os::NOS_Error::IO_Error;
		return this->offset;
	}

	size_t newPosition = attribute == kiv_os::NFile_Seek::Get_Position ? this->offset : offset;

	if (attribute != kiv_os::NFile_Seek::Get_Position)
	{
		switch (attribute)
		{
		case kiv_os::NFile_Seek::Current: newPosition += this->offset; break;
		case kiv_os::NFile_Seek::End:
			if (dir)
			{
				newPosition += strlen(reinterpret_cast<char *>(entries));
			}
			else
			{
				newPosition += mft_item->item_size;
			}
			break;
		case kiv_os::NFile_Seek::Beginning:
		case kiv_os::NFile_Seek::Set_Position:
		case kiv_os::NFile_Seek::Set_Size: break;
		}

		// Je potreba zmenit velikost souboru?
		if (attribute == kiv_os::NFile_Seek::Set_Size || (!dir && newPosition >= mft_item->item_size) || (dir && newPosition >= entries_count))
		{
			if (dir)
			{
				// Adresar je pouze pro cteni!
				result = kiv_os::NOS_Error::IO_Error;
			}
			else
			{
				/*size_t newFileSize = */fs->Resize_File(mft_item, static_cast<int32_t>(newPosition), &result);
			}
		}
	}

	this->offset = static_cast<int32_t>(newPosition);
	return newPosition;
}

/*------------------------------------End FileIO------------------------------------*/


/*------------------------------------Start PipeIO------------------------------------*/

const int PipeIO::BUFFER_SIZE = 2048;

PipeIO::PipeIO(kiv_os::THandle write, kiv_os::THandle read) : s_empty(BUFFER_SIZE), s_full(0), s_mutex(1)
{
	handle = 0;
	file = nullptr;
	mft_item = nullptr;

	write_handle = write;
	read_handle = read;
	writer_closed = false;

	buffer = new char[BUFFER_SIZE];
	memset(buffer, '\0', BUFFER_SIZE);
	act_size = 0;
}

PipeIO::~PipeIO()
{
	delete[] buffer;
}

bool PipeIO::closing(kiv_os::THandle handle)
{
	bool deletePipeIO = handle == read_handle;
	if (!deletePipeIO && !writer_closed)
	{
		writer_closed = true;
		s_full.release();
	}
	return deletePipeIO;
}

void PipeIO::write(kiv_hal::TRegisters &regs)
{
	if (static_cast<kiv_os::THandle>(regs.rdx.x) == write_handle && !writer_closed)
	{
		const char *_buffer = reinterpret_cast<const char *>(regs.rdi.r);
		size_t _buffer_size = static_cast<size_t>(regs.rcx.r);

		if (_buffer_size > BUFFER_SIZE)
		{
			_buffer_size = BUFFER_SIZE;
		}

		// U zapisu vzdy pockat, dokud nebude uvolnen potrebny pocet bytu.
		s_empty.acquire(_buffer_size);
		s_mutex.acquire();
		for (size_t _b_pos = 0; _b_pos < _buffer_size; _b_pos++, act_size++)
		{
			buffer[act_size] = _buffer[_b_pos];
		}
		s_mutex.release();
		s_full.release(_buffer_size);

		regs.rax.r = static_cast<decltype(regs.rax.r)>(_buffer_size);
	}
	else
	{
		regs.flags.carry = true;
		regs.rax.r = static_cast<decltype(regs.rax.r)>(kiv_os::NOS_Error::Permission_Denied);
	}
}

void PipeIO::read(kiv_hal::TRegisters &regs)
{
	if (static_cast<kiv_os::THandle>(regs.rdx.x) == read_handle)
	{
		char *_buffer = reinterpret_cast<char *>(regs.rdi.r);
		size_t _buffer_size = static_cast<size_t>(regs.rcx.r);
		size_t read_chars = 0;

		if (_buffer_size > 0)
		{
			if (_buffer_size > BUFFER_SIZE)
			{
				// Nelze prekrocit limit dany velikosti bufferu.
				_buffer_size = BUFFER_SIZE;
			}

			// U cteni nevyzadovat uplne naplneni bufferu! Pockat alespon na jeden jediny vlozeny znak nebo na ukonceni chodu zapisovatele.
			s_full.acquire();

			// Zjistit pocet polozek v bufferu.
			int chars_inserted = s_full.get_size();
			if (writer_closed)
			{
				// U skonceneho zapisu vzdy nechat pocet plnych polozek vetsi rovno jedne, aby nedochazelo pri opetovnem pokusu o cteni k uviznuti.
				// Takovehle zachazeni se semafory v tomhle pripade nevadi - pouze jeden proces, ktery cte.
				s_full.release();
			}
			else
			{
				// V pripade, ze jeste nedoslo k ukonceni zapisu (zvyseni s_full o jedna), nezapomenout uz na provedene snizeni.
				chars_inserted++;
			}

			if (chars_inserted < _buffer_size)
			{
				// Pocet naplnenych polozek je mensi nez velikost bufferu.
				_buffer_size = chars_inserted;
			}

			if (_buffer_size > 0) // Jestli je neco v bufferu.
			{
				s_full.acquire(writer_closed ? _buffer_size : _buffer_size - 1); // Pouze snizit citac o zbytek - nemelo by dochazet k cekani.
				s_mutex.acquire();
				for (size_t _b_pos = 0, opposite = _buffer_size; _b_pos < BUFFER_SIZE; _b_pos++, opposite++)
				{
					if (_b_pos < _buffer_size && _b_pos < act_size)
					{
						_buffer[_b_pos] = buffer[_b_pos];
						read_chars++;
					}

					if (opposite >= BUFFER_SIZE)
					{
						buffer[_b_pos] = '\0';
					}
					else
					{
						buffer[_b_pos] = buffer[opposite];
					}
				}
				act_size -= read_chars;
				s_mutex.release();
				s_empty.release(read_chars); // Uvolnit misto pouze opravdu prectenych bytu.
			}
		}

		regs.rax.r = static_cast<decltype(regs.rax.r)>(read_chars);
	}
	else
	{
		regs.flags.carry = true;
		regs.rax.r = static_cast<decltype(regs.rax.r)>(kiv_os::NOS_Error::Permission_Denied);
	}
}