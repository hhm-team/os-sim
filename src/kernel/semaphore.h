#pragma once
#include <atomic>
#include <mutex>

class Semaphore {
	
	public:
		Semaphore(int count);
		void down();
		void up();

		/*
		Odebere dany pocet zdroju.
		@param size_t count - pocet zdroju, defaultne 1.
		*/
		void acquire(size_t count = 1);
		
		/*
		Uvolni dany pocet zdroju.
		@param size_t count - pocet zdroju, defaultne 1.
		*/
		void release(size_t count = 1);

		/*
		@return aktualni stav semaforu (pocet dostupnych zdroju).
		*/
		int get_size();

	private:
		std::atomic<int> size;
		std::mutex mtx;
		std::condition_variable cv;
};