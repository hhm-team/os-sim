#pragma once

#include "..\api\api.h"
extern "C" {
	#include "ntfs/ntfs.h"
}

class File_System {
	public:
		//virtual ~File_System() {}

		virtual void Run_Debug_Command(const char* command) = 0;

		// vr�t� ko�enov� adres��
		virtual MftItem* Get_Root_Dir() = 0;
		// najde soubor, path m��e b�t absolutn� i relativn� cesta (v��i working_dir)
		// pokud nic nenajde, vrac� NULL
		virtual MftItem* Find_File(const char *path, MftItem *working_dir) = 0;
		virtual MftItem* Find_File(int32_t uid) = 0;
		// vytvo�� soubor s dan�m n�zvem
		virtual MftItem* Create_File(const char *name, MftItem *dir, uint8_t attributes, kiv_os::NOS_Error *out_error_code) = 0;
		// odstran� soubor nebo pr�zdnou slo�ku
		virtual void Remove_File(MftItem *file, kiv_os::NOS_Error *out_error_code) = 0;
		// nastav� velikost souboru, nelze u slo�ky
		// ur�eno hlavn� pro o�ez�v�n�, p�ed write nen� pot�eba
		// vrac� novou velikost - m��e to b�t m��, pokud nen� dost m�sta
		virtual int32_t Resize_File(MftItem *file, int32_t new_size_in_bytes, kiv_os::NOS_Error *out_error_code) = 0;
		// zap�e bajty do souboru
		// zapisovat do adres��e je zak�z�no
		virtual int32_t Write(MftItem *file, int32_t offset_in_bytes, const void *data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) = 0;
		// na�te bajty ze souboru
		// POZOR: adres�� obsahuje pro ka�d� soubor uid typu int32_t
		virtual int32_t Read(MftItem *file, int32_t offset_in_bytes, void* data, int32_t data_size_in_bytes, kiv_os::NOS_Error *out_error_code) = 0;

		virtual uint32_t Get_Total_Size() = 0;

		virtual void Close() = 0;
};
