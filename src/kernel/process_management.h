#pragma once

#include "..\api\api.h"
#include "signal_management.h"
#include "handle_generator.h"
#include "semaphore.h"
#include "io_management.h"
#include <vector>
#include <thread>
#include <map>
#include <Windows.h>
#include <atomic>

enum Thread_State {
	Running,
	Terminated,
	Blocked
};

class TCB {
public:
	kiv_os::THandle pid;
	std::string id;
	std::shared_ptr<std::thread> thread;
	kiv_os::THandle handle;
	Thread_State state;
	bool primary_thread;
	std::unique_ptr<std::vector<std::string>> waiting_tids;
	Semaphore* semaphore; //semafor ma nastaveny vlakno, ktere na nekoho ceka
};

class PCB {
public:
	PCB();
	~PCB();
	const char *name;
	uint16_t pid;
	std::atomic<uint16_t> exit_code;
	std::unique_ptr < std::vector<std::shared_ptr<TCB>>> threads;
};

class Process_Management {
private:
	std::vector<std::shared_ptr<PCB>> process_table;
	std::map<std::string, std::shared_ptr<TCB>> thread_table;
	std::shared_ptr<Signal_Management> signals; //signal management
	std::atomic<uint16_t> count = 0;
	std::shared_ptr<Handle_Generator> generator; //handle generator
	std::shared_ptr<IO_Management> io_management;
	kiv_os::THandle procfile_handle = kiv_os::Invalid_Handle;

public:
	bool system_terminated;
	/*
	Konstruktor Process_Management.

	@param generator_ptr - pointer na instanci generatoru handlu
	@param io_ptr - pointer na instanci io managementu
	*/
	Process_Management(std::shared_ptr<Handle_Generator> ptr, std::shared_ptr<IO_Management> io_ptr);

	~Process_Management();

	/*
	Zjisti o jaky pozadavek tykajici se procesu se jedna
	a preda jeho vyrizeni spravne funkci.

	@param regs - registry vytvorene systemovym volanim
	@param User_Programs - modul s ecportovanymi funkcemi
	*/
	void handle_process(kiv_hal::TRegisters &regs, HMODULE User_Programs);

	/*
	Vytvori proces a vrati jeho handle

	@param regs - registry vytvorene systemovym volanim
		rcx - kiv_os::NClone::Create_Process
		rdx - pointer na null - terminated string udavajici jmeno programu
		rdi - pointer na null-termined ANSI char string udavajici argumenty programu
		bx - 2x THandle na stdin a stdout, tj. bx.e = (stdin << 16) | stdout

	@param User_Programs - modul s ecportovanymi funkcemi
	@return - handle procesu, jinak kiv_os::Invalid_Handle
	*/
	kiv_os::THandle create_process(kiv_hal::TRegisters &regs, HMODULE User_Programs);

	/*
	Kontrola zda na dane vlakno nekdo neceka. Pokud ano, je siganlizovan
	semafor cekajiciho.

	@param this_id - id vlakna
	*/
	void check_waiting(std::string this_id);

	/*
	Ukonci vlakno. Pokud je vlakno primarni vlakno procesu,
	je zapsan exit code.

	@param err - exit code
	*/
	void terminate(kiv_os::NOS_Error err, std::string thread_id, size_t pid_);

	/*
	Vytvori vlakno a vrati jeho handle.

	@param pid - process id proces, ktery vlastni vlakno
	@param proc - funkce, kterou ma vlakno vykonavat
	@param regs - registry vytvoreny pri volani systemoveho volani
	@param create_thread_syscall - indikace zda se jedna primo o systemove volani vytvoreni vlakna nebo je tvoreno pri inicializaci procesu
	@return - handle vlakna
	*/
	kiv_os::THandle create_thread(uint16_t pid, kiv_os::TThread_Proc proc, kiv_hal::TRegisters & regs, bool create_thread_syscall);

	/*
	Cekani pred prectenim exit code - vsechna vlakna daneho procesu musi dobehnout.

	@param pcb - pcb procesu, na ktery se ceka
	*/
	void wait_before_read_exit(PCB & pcb);

	/*
	Cteni exit code procesu.

	@param regs - registry vytvorene systemovym volanim
		IN	dx - handle procesu/thread jehoz exit code se ma cist
		OUT cx - exitcode
	*/
	void read_exit_code(kiv_hal::TRegisters & regs);

	/*
	Cekani na prvni spusteny program, aby se kernel iihned neukoncil.
	*/
	void wait_for_first_program();

	/*
	Ziskani pidu procesu.

	@return - pid procesu
	*/
	uint16_t get_pid();

	/*
	Zjisteni pidu procesu, kteremu patri dane vlakno.

	@param handle - handle vlakna jehoz procesu je zjistovan pid
	@return - pid procesu
	*/
	uint16_t get_pid(kiv_os::THandle handle);

	/*
	Pridani id cekajiciho vlakna do front cekajicich vlaken, na ktere se ceka.

	@param arr - pole handlu vlaken na ktere se ma cekat
	@param count - pocet vlaken
	*/
	void add_waiting_tid(kiv_os::THandle* arr, uint64_t count, std::string tid);

	/*
	Kontrola, zda nejake z vlaken, na ktere se ma cekat, jiz neskoncilo.

	@param arr - pole vlaken na ktere se ma cekat
	@param count - pocet vlaken v poli
	@return - handle prvniho ukonceneho vlakna
	*/
	kiv_os::THandle check_if_allready_ended(kiv_os::THandle* arr, uint64_t count);

	/*
	Cekani na pole vlaken.

	@param regs - registry vytvorene systemovym volanim
		rcx - kiv_os::NClone::Create_Thread
		rdx - kiv_os::TThread_Proc
		rdi - *data
	@return - handle prvniho signalizovaneho vlakna
	*/
	kiv_os::THandle wait_for(kiv_hal::TRegisters &regs);

	/*
	Vytvoreni procfile pro tasklist.
	*/
	void create_procfile();

	/*
	Zapis udaju do procfile.
	*/
	void write_procfile();

	/*
	Zavolani zapsani do procfile.
	*/
	void write_into_procfile(const char* str);

	/*
	Pretoceni procfile.
	*/
	void rewind_procfile();

	/*
	Zabiti vlaken po dvou sekundach.
	*/
	void thread_killer();
};