#pragma once
#include "../api/api.h"
#include <memory>
#include <vector>

class Signal {
public:
	const kiv_os::TThread_Proc handler;
	const uint8_t number;
	Signal(uint8_t signal_number, kiv_os::TThread_Proc handler);
};

class Signal_Management {
private:
	std::vector<std::shared_ptr<Signal>> registered_handlers;

public:
	Signal_Management();
	~Signal_Management();

	/*
	Pro dany signal jsou zavolany registrovane obsluhy.

	@param signal_number - cislo signalu
	@param regs - registry vytvorene systemovym volanim
	*/
	void handle_signal(uint8_t signal_number, kiv_hal::TRegisters & regs);

	/*
	Registrace handleru na dany signal.

	@param signal_id - id signalu
	@param handler - obsluha signalu
	*/
	void register_signal_handler(kiv_os::NSignal_Id signal_id, kiv_os::TThread_Proc handler);
};