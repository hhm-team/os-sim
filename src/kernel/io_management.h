#pragma once

#include <map>
#include <memory>
#include "../api/api.h"
#include "file_desc_management.h"
#include "file_system_api.h"

// Trida pro manipulaci s FS, prevazne pro systemove volani. Odpovidaji vyctu systemoveho volani v api.h.
class IO_Management
{
private:
	// Tento objekt obsahuje funkce s hlavni cinnosti systemoveho volani.
	std::shared_ptr<File_Desc_Management> file_desc_management;

	// Funkce, ktera nastavy atributy ve strukture kiv_hal::TRegisters v pripade nejake chyby.
	void set_error(kiv_hal::TRegisters &regs, kiv_os::NOS_Error error);

public:
	IO_Management(std::shared_ptr<Handle_Generator> ptr, std::shared_ptr<File_System> fs_ptr);
	~IO_Management();
	
	// Funkce volana v pripade, ze bylo identifikovano systemove volani pro souborovy system.
	void handle_io(kiv_hal::TRegisters &regs);
	
	void open(kiv_hal::TRegisters &regs);
	void read_write(kiv_hal::TRegisters &regs, bool read);
	void seek(kiv_hal::TRegisters &regs);
	void close(kiv_hal::TRegisters &regs);
	void remove(kiv_hal::TRegisters &regs);
	void remove_any_file(kiv_hal::TRegisters &regs); // pro kernel - na smazani systemovych souboru. Nevyuzito.
	void set_dir(kiv_hal::TRegisters &regs);
	void get_dir(kiv_hal::TRegisters &regs);
	void create_pipe(kiv_hal::TRegisters &regs);
};

