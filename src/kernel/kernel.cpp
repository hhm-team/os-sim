#pragma once

#include "kernel.h"
#include "io.h"
#include "process_management.h"
#include "file_system_api.h"
#include "ntfs/ntfs-api.h"
#include "io_management.h"
#include "handle_generator.h"
#include <Windows.h>
#include <memory>
#include <stdio.h>

HMODULE User_Programs;
std::shared_ptr<Handle_Generator> handle_generator = 0;
std::shared_ptr<Process_Management> process_management = 0;
std::shared_ptr<File_System> disk_management = 0;
std::shared_ptr<IO_Management> io_management = 0;

void Initialize_Kernel() {
	User_Programs = LoadLibraryW(L"user.dll");
	handle_generator = std::make_shared<Handle_Generator>();
}

void Shutdown_Kernel() {
	if (disk_management) {
		disk_management->Close();
	}
	FreeLibrary(User_Programs);
}

void __stdcall Sys_Call(kiv_hal::TRegisters &regs) {

	switch (static_cast<kiv_os::NOS_Service_Major>(regs.rax.h)) {
	
		case kiv_os::NOS_Service_Major::File_System:		
			io_management->handle_io(regs);
			break;

		case kiv_os::NOS_Service_Major::Process:
			process_management->handle_process(regs, User_Programs);
			break;

		default:
#ifdef _DEBUG
			if (regs.rax.r == 3) {
				// NTFS debug command
				if (disk_management) {
					char* command = reinterpret_cast<char*>(regs.rdx.r);
					disk_management->Run_Debug_Command(command);
					break;
				}
			}
#endif
			// nezn�m� slu�ba
			regs.flags.carry = true;
			regs.rax.r = static_cast<uint64_t>(kiv_os::NOS_Error::Invalid_Argument);
			break;
	}

}

void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context) {
	Initialize_Kernel();
	kiv_hal::Set_Interrupt_Handler(kiv_os::System_Int_Number, Sys_Call);

	kiv_hal::TRegisters regs;
	for (regs.rdx.l = 0; ; regs.rdx.l++) {
		kiv_hal::TDrive_Parameters params;		
		regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Drive_Parameters);
		regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&params);
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);

		if (!regs.flags.carry) {
			if (params.absolute_number_of_sectors == 36028797018963967) {
				// kdy� chyb� soubor s diskem, tak se boot jeho velikost ur�� na max value
				// z t�to hodnoty se pak vypo�te i absolute_number_of_sectors
				// nep�i�li jsme na lep�� zp�sob jak to o�et�it
			} else {
				disk_management = std::make_shared<File_System_NTFS>(regs.rdx.l, params);
				break;  // TODO: m�me podporovat v�ce disk�?
			}
		}

		if (regs.rdx.l == 255) break;
	}

	io_management = std::make_shared<IO_Management>(handle_generator, disk_management);
	process_management = std::make_shared<Process_Management>(handle_generator, io_management);

	// --- Init shell ---
	kiv_hal::TRegisters registers;
	const kiv_os::THandle in = static_cast<kiv_os::THandle>(0);
	const kiv_os::THandle out = static_cast<kiv_os::THandle>(1);
	registers.rbx.e = (in << 16) | out;
	registers.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>("shell");
	process_management->create_procfile(); // Init procfile
	process_management->create_process(registers, User_Programs);
	process_management->wait_for_first_program(); // wait for shell

	Shutdown_Kernel();
}


void Set_Error(const bool failed, kiv_hal::TRegisters &regs) {
	if (failed) {
		regs.flags.carry = true;
		regs.rax.r = GetLastError();
	}
	else
		regs.flags.carry = false;
}