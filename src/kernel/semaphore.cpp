#include "semaphore.h"

Semaphore::Semaphore(int count) {
	size.store(count);
}

void Semaphore::down() {
	std::unique_lock<std::mutex> lck(mtx);
	if (--size <= 0) {
		cv.wait(lck);
	}
}

void Semaphore::up() {
	std::unique_lock<std::mutex> lck(mtx);
	if (++size > 0) {
		cv.notify_one();
	}
}

void Semaphore::acquire(size_t count)
{
	if (count < 1)
		return;

	std::unique_lock<std::mutex> lck(mtx);
	while ((size - static_cast<int>(count)) < 0)
	{
		cv.wait(lck);
	}

	size -= static_cast<int>(count);

	if (size > 0)
	{
		cv.notify_one();
	}

	// mutex unlock automatically.
}

void Semaphore::release(size_t count)
{
	std::unique_lock<std::mutex> lck(mtx);
	if (count < 1)
		return;

	size += static_cast<int>(count);
	//size.fetch_add(static_cast<int>(count));
	cv.notify_one();
}

int Semaphore::get_size()
{
	std::unique_lock<std::mutex> lck(mtx);
	return size;
}