#include "signal_management.h"

Signal_Management::Signal_Management() {
	registered_handlers = {};
}

Signal_Management::~Signal_Management() {
	registered_handlers.clear();
}

/*
Pro dany signal jsou zavolany registrovane obsluhy.

@param signal_number - cislo signalu
@param regs - registry vytvorene systemovym volanim
*/
void Signal_Management::handle_signal(uint8_t signal_number, kiv_hal::TRegisters & regs) {
	for (auto & sig : registered_handlers) {
		if (sig->number == signal_number) {
			sig->handler(regs);
		}
	}
}

/*
Registrace handleru na dany signal.

@param signal_id - id signalu
@param handler - obsluha signalu
*/
void Signal_Management::register_signal_handler(kiv_os::NSignal_Id signal_id, kiv_os::TThread_Proc handler) {
	uint8_t signal_number = static_cast<uint8_t>(signal_id);
	auto sig = std::make_shared<Signal>(signal_number, handler);
	registered_handlers.push_back(sig);
}

Signal::Signal(uint8_t signal_number, kiv_os::TThread_Proc handler) : number(signal_number), handler(handler)
{ }
