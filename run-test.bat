@echo off

echo ===========================================================================
echo Running test: %1

mkdir compiled > nul 2> nul
copy test\* compiled > nul
xcopy /y %1 compiled > nul

echo ===========================================================================

cd compiled

type "test-input.txt" | find /v "" | boot.exe
REM this also converts lf to crlf

cd ..
